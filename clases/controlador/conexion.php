<?php 
class conexion 
{ 
private $_host = '10.10.230.60'; 
 
private $_baseDatos = 'robot_chilecompra';
private $_usuario = 'eduardo';
private $_pass = 'dimsql'; 
private $_coleccion = array();
private $_db;
private $_dbExterno;


private function _conectar()
{
    $this->_db = pg_connect('host='.$this->_host.' dbname='.$this->_baseDatos.' user='.$this->_usuario.' password='.$this->_pass.'');
 
	
}

private function _desconectar()
{
    pg_close($this->_db);
}


private function _conectarExterno($motor, $servidor, $bd, $usuario, $pass, $puerto)
{

	switch ($motor) {
		case "PostgreSQL":
	    $this->_dbExterno = pg_connect('host='.$servidor.' dbname='.$bd.' user='.$usuario.' password='.$pass.'');
	    break;
	}
	
}

private function _desconectarExterno($motor)
{
		switch ($motor) {
		case "PostgreSQL":
	    pg_close($this->_dbExterno);
	    break;
    }
}

public function selectSimple($query) 
    { 
 	$this->_conectar();
 	  $result = pg_query($query);
 	  if (pg_num_rows($result) > 0){
		 $row = pg_fetch_array($result);
	 	return $row;
	 	}else{
	 	return "LaConsultaNoTieneRegistros";	
	 	}
	$this->_desconectar();
    } 


public function selectMultiple($query) 
    { 
 	$this->_conectar();
 	  $result = pg_query($query);
 	  if (pg_num_rows($result) > 0){
		  while($row = pg_fetch_array($result)) {
		  	array_push($this->_coleccion, $row);
		  	}
	 	return $this->_coleccion;
	 	}else{
	 	return "LaConsultaNoTieneRegistros";	
	 	}
	$this->_desconectar();
    } 

public function ejecuta($query) 
    { 
 	$this->_conectar();
 	  $result = pg_query($query);
 	  if (pg_num_rows($result) > 0){
		 $row = pg_fetch_array($result);
	 	return $row;
	 	}else{
	 	return "go";	
	 	}
	$this->_desconectar();
    } 


public function insertSimple($query) 
    { 
 	$this->_conectar();
    $result = pg_query($query);
    $row = pg_fetch_array($result);
    $this->_desconectar();
	return $row[0];
    } 

public function insert($query) 
    { 
 	$this->_conectar();
    $result = pg_query($query);
    $this->_desconectar();
	return $result;
    } 

public function selectSimpleExterno($query, $motor, $servidor, $bd, $usuario, $pass, $puerto) 
    { 
 	$this->_conectarExterno($motor, $servidor, $bd, $usuario, $pass, $puerto);
 	  $result = pg_query($query);
 	  if (pg_num_rows($result) > 0){
		 $row = pg_fetch_array($result);
	 	return $row;
	 	}else{
	 	return "LaConsultaNoTieneRegistros";	
	 	}
	$this->_desconectarExterno($motor);
    } 

 
public function selectMultipleExterno($query, $motor, $servidor, $bd, $usuario, $pass, $puerto) 
    { 
 	$this->_conectarExterno($motor, $servidor, $bd, $usuario, $pass, $puerto);
 	  $result = pg_query($query);
 	  if (pg_num_rows($result) > 0){
		  while($row = pg_fetch_array($result)) {
		  	array_push($this->_coleccion, $row);
		  	}
	 	return $this->_coleccion;
	 	}else{
	 	return "LaConsultaNoTieneRegistros";	
	 	}
	$this->_desconectarExterno($motor);
    } 

}




