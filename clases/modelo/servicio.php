<?php 
class servicio
{ 
private $_pass; 
private $_rut; 
private $_ordenDeCompra; 
private $_fecha;
private $_descripcion; 
private $_direNom; 
private $_direCalle;
private $_direCiudad;
private $_direRegion; 
private $_nombre;
private $_correo; 
private $_telefono;
private $_neto;
private $_bruto;
private $_descuentototal;
private $_direNomF; 
private $_direCalleF;
private $_direCiudadF;
private $_direRegionF; 

private $_idDetalle;
private $_texto; 
private $_codigo;
private $_detalle;
private $_cantidad; 
private $_precio;
private $_moneda; 
private $_descuento;
private $_codigoChc;

private $_tipoListado;
private $_idPedido;
private $_categoria;
private $_idMotivo;
private $_cencos;
private $_idChc; 
private $_idDimerc;
private $_unidad;  
private $_tipoCodigo;
private $_rutCli;
private $_rutDesc;
private $_tipo;
private $_ejecutivo;
private $_cencosTexto;

private $_rutEje; 
private $_usuEje;
private $_rs;
private $_numTelemarketing;

private $_leidas;
private $_sinAsignar;
private $_yaHecha; 
private $_yaHechaNtaVta; 
private $_totalplata;
private $_noAplica;
private $_error;
private $_asignada;
private $_tiempo;

private $_numero;
private $_desde;
private $_hasta;
private $_xmlCotizacion;

private $_detalleDuplicado;
private $_familia;
private $_nuevoTexto;
 
public function validaInicioSesion($rut, $pass) 
    { 
         $this->_rut = $rut; 
         $this->_pass = $pass; 
        return $this->_validaLogInConBd(); 
    }

public function vincular($id, $rut, $tipo) 
    { 
         $this->_rut = $rut; 
         $this->_idPedido = $id;
         $this->_tipo = $tipo; 
        return $this->_vincularConBd(); 
    }

public function generarEstadisticaOC($desde, $hasta) 
    { 
         $this->_desde = $desde; 
         $this->_hasta = $hasta;
        return $this->_generarEstadisticaOCConBd(); 
    }

public function aceptarOrden($id, $rut, $cc) 
    { 
         $this->_rut = $rut; 
         $this->_idPedido = $id;
         $this->_cencos = $cc;
        return $this->_aceptarOrdenConBd(); 
    }

public function guardarNuevaRelacionOC($iddetalle, $codDimerc, $unidad) 
    { 
         $this->_idDetalle = $iddetalle; 
         $this->_idDimerc = $codDimerc;
         $this->_unidad = $unidad;
        return $this->_guardarNuevaRelacionOCConBd(); 
    }

public function reemplazarProducto($iddetalle, $codDimerc, $codDimercOld, $tipo, $unidad, $nuevoTexto) 
    { 
         $this->_idDetalle = $iddetalle; 
         $this->_idDimerc = $codDimerc;
         $this->_tipo = $tipo;
         $this->_idDimercOld = $codDimercOld;
         $this->_unidad = $unidad;
         $this->_nuevoTexto = $nuevoTexto;
        return $this->_reemplazarProductoConBd(); 
    }

public function rechazar($id, $rut, $idmotivo, $com, $cc, $cctext) 
    { 
         $this->_rut = $rut; 
         $this->_idPedido = $id;
         $this->_idMotivo = $idmotivo;  
         $this->_comentarioEjecutivo = $com;
         $this->_cencos = $cc;
         $this->_cencosTexto = $cctext;
        return $this->_rechazarConBd(); 
    }

public function guardaFilaCodigo($idchc, $iddimerc, $unidad, $tipo) 
    { 
         $this->_idChc = $idchc; 
         $this->_idDimerc = $iddimerc;
         $this->_unidad = $unidad;  
         $this->_tipoCodigo = $tipo;
        return $this->_guardaFilaCodigoConBd(); 
    }

public function detalleOC($id) 
    { 
         $this->_idPedido = $id; 
        return $this->_detalleOCConBd(); 
    }

public function traerListadoFamilia() 
    { 
        return $this->_traerListadoFamiliaConBd(); 
    }

public function dameSugrenciaProduto($fa, $pre, $rut, $cc) 
    { 
        $this->_familia = $fa; 
        $this->_precio = $pre; 
        $this->_rut = $rut; 
        $this->_cencos = $cc; 
        return $this->_dameSugrenciaProdutoConBd(); 
    }

public function traerTodoDeEstaFamilia($fa, $pre, $rut, $cc) 
    { 
        $this->_familia = $fa; 
        $this->_precio = $pre; 
        $this->_rut = $rut; 
        $this->_cencos = $cc; 
        return $this->_traerTodoDeEstaFamiliaConBd(); 
    }


public function buscarOCtelemarketing($num) 
    { 
        $this->_numero = $num; 
        return $this->_buscarOCtelemarketingConBd(); 
    }

public function traerOCahora($oc) 
    { 
        $this->_ordenDeCompra = $oc; 
        return $this->_traerOCahoraConBd(); 
    }

public function borrarOCtelemarketing($num) 
    { 
        $this->_numero = $num; 
        return $this->_borrarOCtelemarketingConBd(); 
    }

public function deshacerOrden($id, $tele) 
    { 
         $this->_idPedido = $id; 
         $this->_numTelemarketing = $tele;
        return $this->_deshacerOrdenConBd(); 
    }

public function insertarPedido($oc, $fe, $ref, $rut, $direNom, $direCalle, $direCiudad, $direRegion, $nom, $corr, $tel, $txt, $cod, $det, $cant, $prec, $mon, $des, $neto, $bruto, $dscto, $direNomF, $direCalleF, $direCiudadF, $direRegionF)
    { 
        $this->_ordenDeCompra = $oc; 
        $this->_fecha = $fe;
        $this->_descripcion = $ref; 
        $this->_rut = $rut;
        $this->_direNom = $direNom; 
        $this->_direCalle = $direCalle;
        $this->_direCiudad = $direCiudad;
        $this->_direRegion = $direRegion; 
        $this->_nombre = $nom;
        $this->_correo = $corr; 
        $this->_telefono = $tel;
        $this->_texto = $txt;
        $this->_codigo = $cod;
        $this->_detalle = $det;
        $this->_cantidad = $cant;
        $this->_precio = $prec;
        $this->_moneda = $mon;
        $this->_descuento = $des;
        $this->_neto = $neto;
        $this->_bruto = $bruto;
        $this->_descuentototal = $dscto;
        $this->_direNomF = $direNomF; 
        $this->_direCalleF = $direCalleF;
        $this->_direCiudadF = $direCiudadF;
        $this->_direRegionF = $direRegionF; 
        return $this->_insertarPedidoConBd(); 
    }

public function guardarLicitacion($num, $tipo, $nom, $des, $org, $reg, $fpu, $fce, $despro, $onu, $unidad, $cant, $gen, $n1, $n2, $n3)
    { 
        $this->_numero = $num; 
        $this->_tipo = $tipo;
        $this->_nombre = $nom; 
        $this->_descripcion = $des;
        $this->_organizacion = $org; 
        $this->_region = $reg;
        $this->_fpubli = $fpu;
        $this->_fcierre = $fce; 
        $this->_desPro = $despro;
        $this->_onu = $onu; 
        $this->_unidad = $unidad;
        $this->_cantidad = $cant;
        $this->_generico = $gen;
        $this->_n1 = $n1;
        $this->_n2 = $n2;
        $this->_n3 = $n3;
        return $this->_guardarLicitacionConBd(); 
    }
 
public function listadoPedido($tipo, $cat)
    { 
        $this->_tipoListado = $tipo; 
        $this->_categoria = $cat; 
        return $this->_listadoPedidoConBd(); 
    }
 
public function listadoMotivos()
    { 
        return $this->_listadoMotivosConBd(); 
    }

public function borrarRelacionDeshabilitar($tipo)
    {   
       $this->_tipoListado = $tipo; 
       $this->_borrarRelacionDeshabilitarConBd(); 
    }

public function habilitarEnvio()
    { 
       $this->_habilitarEnvioConBd(); 
    }

public function tipoWs()
    { 
       return $this->_tipoWsConBd(); 
    }

public function listadoCodigo()
    { 
       return $this->_listadoCodigoConBd(); 
    }

public function cambiarAsignacion($id, $rutcli, $rutdesc, $tipo) 
    { 
         $this->_rutCli = $rutcli;
         $this->_rutDesc = $rutdesc;  
         $this->_idPedido = $id;
         $this->_tipo = $tipo; 
        return $this->_cambiarAsignacionConBd(); 
    }

public function accionRechazo($id, $eje, $tipo, $cc) 
    { 
         $this->_rut = $eje;
         $this->_idPedido = $id;
         $this->_tipo = $tipo; 
         $this->_cencos = $cc;
        return $this->_accionRechazoConBd(); 
    }

public function listadoSinProcesar()
    { 
        return $this->_listadoSinProcesarConBd(); 
    }

public function limpiarPendientes()
    { 
        return $this->_limpiarYaHechosConBd(); 
    }

public function limpiarPendientesMasivo()
    { 
        return $this->_limpiarYaHechosMasivoConBd(); 
    }

public function borrarOC($id)
    { 
        $this->_idPedido = $id;
        return $this->_borrarOCConBd(); 
    }

public function estadoCliente($rut, $cc)
    { 
        $this->_rutCli = $rut;
        $this->_cencos = $cc;
        return $this->_estadoClienteConBd(); 
    }


public function procesarOC($id, $rutDescr, $rutEjecutivo, $usuarioEjecutivo, $rs, $oc)
    { 
         $this->_idPedido = $id;
         $this->_rutDesc = $rutDescr;
         $this->_rutEje = $rutEjecutivo; 
         $this->_usuEje = $usuarioEjecutivo;
         $this->_rs = $rs;
         $this->_ordenDeCompra = $_ordenDeCompra;
        return $this->_procesarOCConBd(); 
    }

public function guardarRobot($cantLeidas, $cantSinAsignar, $cantYaHecha, $cantNoAplica, $error, $cantAsignada, $cantYaHechaNtavta, $totalplata, $tiempo, $tipo)
    { 
         $this->_leidas = $cantLeidas;
         $this->_sinAsignar = $cantSinAsignar;
         $this->_yaHecha = $cantYaHecha; 
         $this->_noAplica = $cantNoAplica;
         $this->_error = $error;
         $this->_asignada = $cantAsignada;
         $this->_yaHechaNtaVta = $cantYaHechaNtavta;
         $this->_totalplata = $totalplata;
         $this->_tiempo = $tiempo;
         $this->_tipo = $tipo;
        return $this->_guardarRobotConBd(); 
    }

public function guardarRobotCompetencia($cantLeidas, $tiempo)
    { 
         $this->_leidas = $cantLeidas;
         $this->_tiempo = $tiempo;
         return $this->_guardarRobotCompetenciaConBd(); 
    }

public function dameEstado($rut, $cc)
    { 
         $this->_rutCli = $rut; 
         $this->_cencos = $cc;
        return $this->_estadoClienteConBd(); 
    }


public function guardarCotizacion($xmlCoti)
    { 
         $this->_xmlCotizacion = $xmlCoti; 
        return $this->_guardarCotizacionConBd(); 
    }

public function guardarOcCompetencia($id, $xmlCoti)
    { 
         $this->_idCompetencia = $id; 
         $this->_xmlOC = $xmlCoti; 
        return $this->_guardarOcCompetenciaConBd(); 
    }


 function quitar_tildes($cadena) {
$no_permitidas= array ("'", '"', "N°","n°", "Nº", "nº", "á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $permitidas= array ("\'", '\"', "N ","n ", "N ","n ","a","e","i","o","u","A","E","I","O","U","n","N","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
$texto = str_replace($no_permitidas, $permitidas ,$cadena);
return $texto;
}




private function _guardarOcCompetenciaConBd() 
    { 
  
$netoCalculado =0;
 
    require_once "../clases/controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
 $respuesta="";
$this->_ordenDeCompra = $this->_xmlOC->Listado->OrdenCompra->Codigo; 

 
 

 

        $query = "SELECT id, idestado FROM pedidocompetencia WHERE trim(buyerordernumber) = '".trim($this->_ordenDeCompra)."'";
        $resultado = $conect->selectSimple($query);
        $estadoCotizacion = $this->_xmlOC->Listado->OrdenCompra->CodigoEstado;  
        if ($resultado!="LaConsultaNoTieneRegistros"){

            
            if ($estadoCotizacion!=$resultado["idestado"]){
                 $query2 = "UPDATE pedidocompetencia SET idestado = ".$estadoCotizacion." WHERE id =".$resultado[0]."";
                 $conect->ejecuta($query2);

            }

            return "Ya encontrada";
            exit();
        }

 

 
    $this->_fecha = $this->_xmlOC->Listado->OrdenCompra->Fechas->FechaCreacion;  
    $this->_descripcion = $this->_xmlOC->Listado->OrdenCompra->Descripcion;    
    $this->_rut = $this->_xmlOC->Listado->OrdenCompra->Comprador->RutUnidad;  
    $this->_direNom = $this->_xmlOC->Listado->OrdenCompra->Comprador->NombreUnidad;  
    $this->_direCalle = $this->_xmlOC->Listado->OrdenCompra->Comprador->DireccionUnidad;    
    $this->_direCiudad = $this->_xmlOC->Listado->OrdenCompra->Comprador->ComunaUnidad;     
    $this->_direRegion = $this->_xmlOC->Listado->OrdenCompra->Comprador->RegionUnidad;    
    $this->_nombre = $this->_xmlOC->Listado->OrdenCompra->Comprador->NombreContacto;  
    $this->_correo = $this->_xmlOC->Listado->OrdenCompra->Comprador->MailContacto;  
    $this->_telefono = $this->_xmlOC->Listado->OrdenCompra->Comprador->FonoContacto;
    $this->_neto = $this->_xmlOC->Listado->OrdenCompra->TotalNeto;  
    $this->_bruto = $this->_xmlOC->Listado->OrdenCompra->Total;    
    $this->_descuentototal = $this->_xmlOC->Listado->OrdenCompra->Descuentos;    
    $this->_fechaReferencia = $this->_xmlOC->Listado->OrdenCompra->Fechas->FechaEnvio;     
    $this->_rs = $this->_xmlOC->Listado->OrdenCompra->Comprador->NombreOrganismo;  
    $separarRut = explode("-", $this->_rut);
    $this->_rut = str_replace(".", "", $separarRut[0]);
    $dv = $separarRut[1];
    

 
 
 


       $query = "INSERT INTO pedidocompetencia(idestado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, nombre, correo, telefono, totalneto, razonsocial, totalbruto, descuento, fechareferencia, idcompetencia) VALUES(".$estadoCotizacion.", '".$this->_ordenDeCompra."', '".$this->_fecha."', '".$this->quitar_tildes($this->_descripcion)."', ".$this->_rut.", '".$dv."', '".$this->quitar_tildes($this->_direNom)."', '".$this->quitar_tildes($this->_direCalle)."', '".$this->quitar_tildes($this->_direCiudad)."', '".$this->quitar_tildes($this->_direRegion)."', '".$this->_nombre."', '".$this->_correo."', '".$this->_telefono."', '".$this->_neto."', '".$this->quitar_tildes($this->_rs)."', '".$this->_bruto."', '".$this->_descuentototal."', '".$this->_fechaReferencia."', ".$this->_idCompetencia.")";
       $resultado = $conect->insertRetornandoId($query);
    if ($resultado > 0){
                $listadoDetalle = $this->_xmlOC->Listado->OrdenCompra->Items->Listado->Item;  
                 for ($i=0; $i < count($listadoDetalle); $i++) { 
              
                   $this->_texto = $listadoDetalle[$i]->Producto;
                   $this->_codigo = $listadoDetalle[$i]->CodigoProducto;
                   $this->_codigo = str_replace("Código: ", "", $this->_codigo);
                   $this->_detalle = $listadoDetalle[$i]->EspecificacionComprador;
                   $this->_cantidad = $listadoDetalle[$i]->Cantidad;
                   $this->_precio = $listadoDetalle[$i]->PrecioNeto;
                   $this->_moneda = $listadoDetalle[$i]->Moneda;
                   $this->_codigoChc = explode(")", $this->_detalle);
                   $this->_codigoChc = trim(str_replace("(", "", $this->_codigoChc[0]));
                   $conect2 = new conexionMySql(); 
                   $query2 = "INSERT INTO detallecompetencia(idpedido, idtexto, idcodigo, descripcion, cantidad, precio, moneda, codigochc) VALUES(".$resultado.", '".$this->quitar_tildes($this->_texto)."', '".$this->_codigo."', '".$this->quitar_tildes($this->_detalle)."', '".$this->_cantidad."', '".$this->_precio."', '".$this->_moneda."', '".$this->_codigoChc."')";
                   $resultado2 = $conect2->ejecuta($query2);

                 }
 
                return "Guardado";

        }else{
        return $query;
        return "error al insertar cabecera de oc ".$this->_ordenDeCompra;

       }
  
 
}










private function _guardarCotizacionConBd() 
    { 
  
$netoCalculado =0;
 
    require_once "../clases/controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
 $respuesta="";
$this->_ordenDeCompra = $this->_xmlCotizacion->OrderHeader->OrderNumber->BuyerOrderNumber; 

$idestado = 0;


    $noc = explode("-", $this->_ordenDeCompra);
    if (substr (trim($noc[2]), 0, strlen(trim($noc[2])) - 2) =="SE"){
        return 0;
        exit();
    }

        $query = "SELECT id, idestado FROM pedido WHERE trim(buyerordernumber) = '".$this->_ordenDeCompra."'";
        $resultado = $conect->selectSimple($query);

        if ($resultado!="LaConsultaNoTieneRegistros"){

            $estadoCotizacion = $this->_xmlCotizacion->OrderSummary->SummaryNote; 
            if ((trim($estadoCotizacion)=="OC Cancelada") AND ($resultado[1]==2)){
                 $query2 = "UPDATE pedido SET idestado = 9 WHERE id =".$resultado[0]."";
                 $conect->ejecuta($query2);

            }

            return 1;
            exit();
        }

require_once "../clases/controlador/conexionOracle.php"; 
  $conectO = new conexionOracle(); 
    $query2 = "SELECT * FROM EN_NOTAVTA WHERE trim(docaso)= '".$this->_ordenDeCompra."'";
    $resultado = $conectO->selectSimple($query2);
if (count($resultado)!=0){
$idestado = 8;
 $respuesta=2;
}

 
    $this->_fecha = $this->_xmlCotizacion->OrderHeader->OrderDates->PromiseDate;   
    $this->_descripcion = $this->_xmlCotizacion->OrderHeader->OrderReferences->OtherOrderReferences->ReferenceCoded->ReferenceDescription;      
    $this->_rut = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->PartyID->Ident; 
    $this->_direNom = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->Name1;
    $this->_direCalle = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->Street;    
    $this->_direCiudad = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->District;    
    $this->_direRegion = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->City;   
    $this->_nombre = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->PrimaryContact->ContactName;    
    $correoyfono = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->PrimaryContact->ListOfContactNumber->ContactNumber;  
    $this->_correo = $correoyfono[0]->ContactNumberValue;    
    $this->_telefono = $correoyfono[1]->ContactNumberValue;    
    $this->_direCalleF = $this->_xmlCotizacion->OrderHeader->OrderParty->BillToParty->NameAddress->Street;    
    $this->_neto = $this->_xmlCotizacion->OrderSummary->OrderSubTotal->MonetaryAmount;
    $this->_bruto = $this->_xmlCotizacion->OrderSummary->OrderTotal->MonetaryAmount;    
    $this->_descuentototal = $this->_xmlCotizacion->OrderSummary->AllowOrChargeSummary->TotalAllowOrCharge->SummaryAllowOrCharge->MonetaryAmount;   
    $this->_fechaReferencia = $this->_xmlCotizacion->OrderHeader->OrderReferences->OtherOrderReferences->ReferenceCoded->PrimaryReference->RefDate;   

    $separarRut = explode("-", $this->_rut);
    $this->_rut = str_replace(".", "", $separarRut[0]);
    $dv = $separarRut[1];
    

    $this->_rutDesc =  $this->buscarRutEnDescripcion($this->_descripcion, $this->_rut);


           if (($this->_rutDesc!="No hay!") and ($this->_rutDesc!="Encontrado, pero es un error conocido!")){
             $rutyusuarioejecutivo = $this->dameEjecutivo(substr ($this->_rutDesc, 0, strlen($this->_rutDesc) - 1));
             $this->_rs =  $this->dameRs(substr ($this->_rutDesc, 0, strlen($this->_rutDesc) - 1));
             if (count($rutyusuarioejecutivo)>0){
                $rutyusuarioejecutivo = $rutyusuarioejecutivo[0];
                $this->_rutEje = $rutyusuarioejecutivo["RUTVENDEDOR"];
                $this->_usuEje = $rutyusuarioejecutivo["USUARIO"];
             }else{
                $this->_rutEje = "";
                $this->_usuEje = "";
             }
          }else{
             $this->_rs =  $this->dameRs($this->_rut);
             $rutyusuarioejecutivo = $this->dameEjecutivo($this->_rut);
             if (count($rutyusuarioejecutivo)>0){
                $rutyusuarioejecutivo = $rutyusuarioejecutivo[0];
                $this->_rutEje = $rutyusuarioejecutivo["RUTVENDEDOR"];
                $this->_usuEje = $rutyusuarioejecutivo["USUARIO"];
             }else{
                $this->_rutEje = "";
                $this->_usuEje = "";
             }
          }

          if ($idestado == 0){
              if (strlen($this->_rutEje) > 5){
                $respuesta=3;
                $idestado = 2;
              }else{
                $respuesta=4;
                $idestado = 1;
              }
          }



       $query = "INSERT INTO pedido(idestado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, nombre, correo, telefono, direccionfactura, totalneto, rutejecutivo, usuarioejecutivo, rutdescripcion, idmotivo, razonsocial, totalbruto, descuento, fechareferencia) VALUES(".$idestado.", '".$this->_ordenDeCompra."', '".$this->_fecha."', '".$this->quitar_tildes($this->_descripcion)."', ".$this->_rut.", '".$dv."', '".$this->quitar_tildes($this->_direNom)."', '".$this->quitar_tildes($this->_direCalle)."', '".$this->quitar_tildes($this->_direCiudad)."', '".$this->quitar_tildes($this->_direRegion)."', '".$this->_nombre."', '".$this->_correo."', '".$this->_telefono."', '".$this->quitar_tildes($this->_direCalleF)."', '".$this->_neto."', '".$this->_rutEje."', '".$this->_usuEje."', '".substr($this->_rutDesc, 0, strlen($this->_rutDesc) - 1)."', 1, '".$this->_rs."', '".$this->_bruto."', '".$this->_descuentototal."', '".$this->_fechaReferencia."')";
       $resultado = $conect->insertRetornandoId($query);
    if ($resultado > 0){
                $listadoDetalle = $this->_xmlCotizacion->OrderDetail->ListOfItemDetail->ItemDetail;
                 for ($i=0; $i < count($listadoDetalle); $i++) { 
              
                   $this->_texto = $listadoDetalle[$i]->BaseItemDetail->ItemIdentifiers->PartNumbers->SellerPartNumber->PartIDExt;
                   $this->_codigo = $listadoDetalle[$i]->BaseItemDetail->ItemIdentifiers->PartNumbers->BuyerPartNumber->PartIDExt;
                   $this->_codigo = str_replace("Código: ", "", $this->_codigo);
                   $this->_detalle = $listadoDetalle[$i]->BaseItemDetail->ItemIdentifiers->ItemDescription;
                   $this->_cantidad = $listadoDetalle[$i]->BaseItemDetail->TotalQuantity->QuantityValue;
                   $this->_precio = $listadoDetalle[$i]->PricingDetail->ListOfPrice->Price->UnitPrice->UnitPriceValue;
                   $this->_moneda = $listadoDetalle[$i]->PricingDetail->ItemAllowancesOrCharges->AllowOrCharge->TypeOfAllowanceOrCharge->MonetaryValue->Currency->CurrencyCoded;
                   $this->_codigoChc = explode(")", $this->_detalle);
                   $this->_codigoChc = trim(str_replace("(", "", $this->_codigoChc[0]));
                   $this->_correlativo = $listadoDetalle[$i]->BaseItemDetail->LineItemNum->SellerLineItemNum;
                   $conect2 = new conexionMySql(); 
                   $query2 = "INSERT INTO detalle(idpedido, idtexto, idcodigo, descripcion, cantidad, precio, moneda, codigochc, correlativo) VALUES(".$resultado.", '".$this->quitar_tildes($this->_texto)."', '".$this->_codigo."', '".$this->quitar_tildes($this->_detalle)."', '".$this->_cantidad."', '".$this->_precio."', '".$this->_moneda."', '".$this->_codigoChc."', '".$this->_correlativo."')";
                   $resultado2 = $conect2->ejecuta($query2);

                   $netoCalculado = $netoCalculado + ($this->_precio*$this->_cantidad);
                }

                $query2 = "UPDATE pedido SET totalnetocalculado = '".$netoCalculado."' WHERE id =".$resultado."";
                $conect->ejecuta($query2);

                return $respuesta;

        }else{
        $query = "INSERT INTO pedido_error(buyerordernumber, desde) VALUES('".$this->_ordenDeCompra."', 'Robot')";
       $resultado = $conect->insertRetornandoId($query);
        return "error al insertar cabecera de oc ".$this->_ordenDeCompra;

       }
  
 
}


private function _guardarCotizacionDesdePortal() 
    { 
  
$netoCalculado =0;
 
    require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
 $respuesta="";
$this->_ordenDeCompra = $this->_xmlCotizacion->OrderHeader->OrderNumber->BuyerOrderNumber; 

$idestado = 0;


    $noc = explode("-", $this->_ordenDeCompra);
    if (substr (trim($noc[2]), 0, strlen(trim($noc[2])) - 2) =="SE"){
        return 0;
        exit();
    }

        $query = "SELECT id, usuarioejecutivo, fechahoracreacion FROM pedido WHERE trim(buyerordernumber) = '".$this->_ordenDeCompra."'";
        $resultado = $conect->selectSimple($query);

        if ($resultado!="LaConsultaNoTieneRegistros"){
            return "1-".$resultado["usuarioejecutivo"];
            exit();
        }

require_once "../controlador/conexionOracle.php"; 
  $conectO = new conexionOracle(); 
    $query2 = "SELECT * FROM EN_NOTAVTA WHERE trim(docaso)= '".$this->_ordenDeCompra."'";
    $resultado = $conectO->selectSimple($query2);
if (count($resultado)!=0){
$idestado = 8;
 $respuesta=2;
}

 
    $this->_fecha = $this->_xmlCotizacion->OrderHeader->OrderDates->PromiseDate;   
    $this->_descripcion = $this->_xmlCotizacion->OrderHeader->OrderReferences->OtherOrderReferences->ReferenceCoded->ReferenceDescription;      
    $this->_rut = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->PartyID->Ident; 
    $this->_direNom = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->Name1;
    $this->_direCalle = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->Street;    
    $this->_direCiudad = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->District;    
    $this->_direRegion = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->NameAddress->City;   
    $this->_nombre = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->PrimaryContact->ContactName;    
    $correoyfono = $this->_xmlCotizacion->OrderHeader->OrderParty->BuyerParty->PrimaryContact->ListOfContactNumber->ContactNumber;  
    $this->_correo = $correoyfono[0]->ContactNumberValue;    
    $this->_telefono = $correoyfono[1]->ContactNumberValue;    
    $this->_direCalleF = $this->_xmlCotizacion->OrderHeader->OrderParty->BillToParty->NameAddress->Street;    
    $this->_neto = $this->_xmlCotizacion->OrderSummary->OrderSubTotal->MonetaryAmount;
    $this->_bruto = $this->_xmlCotizacion->OrderSummary->OrderTotal->MonetaryAmount;    
    $this->_descuentototal = $this->_xmlCotizacion->OrderSummary->AllowOrChargeSummary->TotalAllowOrCharge->SummaryAllowOrCharge->MonetaryAmount;   
    $this->_fechaReferencia = $this->_xmlCotizacion->OrderHeader->OrderReferences->OtherOrderReferences->ReferenceCoded->PrimaryReference->RefDate;   

    $separarRut = explode("-", $this->_rut);
    $this->_rut = str_replace(".", "", $separarRut[0]);
    $dv = $separarRut[1];
    

    $this->_rutDesc =  $this->buscarRutEnDescripcionDesdePortal($this->_descripcion, $this->_rut);


           if (($this->_rutDesc!="No hay!") and ($this->_rutDesc!="Encontrado, pero es un error conocido!")){
             $rutyusuarioejecutivo = $this->dameEjecutivoDesdePortal(substr ($this->_rutDesc, 0, strlen($this->_rutDesc) - 1));
             $this->_rs =  $this->dameRsDesdePortal(substr ($this->_rutDesc, 0, strlen($this->_rutDesc) - 1));
             if (count($rutyusuarioejecutivo)>0){
                $rutyusuarioejecutivo = $rutyusuarioejecutivo[0];
                $this->_rutEje = $rutyusuarioejecutivo["RUTVENDEDOR"];
                $this->_usuEje = $rutyusuarioejecutivo["USUARIO"];
             }else{
                $this->_rutEje = "";
                $this->_usuEje = "";
             }
          }else{
             $this->_rs =  $this->dameRsDesdePortal($this->_rut);
             $rutyusuarioejecutivo = $this->dameEjecutivoDesdePortal($this->_rut);

             if (count($rutyusuarioejecutivo)>0){
                $rutyusuarioejecutivo = $rutyusuarioejecutivo[0];
                $this->_rutEje = $rutyusuarioejecutivo["RUTVENDEDOR"];
                $this->_usuEje = $rutyusuarioejecutivo["USUARIO"];
              }else{
                $this->_rutEje = "";
                $this->_usuEje = "";
             }
          }

          if ($idestado == 0){
              if (strlen($this->_rutEje) > 5){
                $respuesta=3;
                $idestado = 2;
              }else{
                $respuesta=4;
                $idestado = 1;
              }
          }



       $query = "INSERT INTO pedido(idestado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, nombre, correo, telefono, direccionfactura, totalneto, rutejecutivo, usuarioejecutivo, rutdescripcion, idmotivo, razonsocial, totalbruto, descuento, fechareferencia) VALUES(".$idestado.", '".$this->_ordenDeCompra."', '".$this->_fecha."', '".$this->quitar_tildes($this->_descripcion)."', ".$this->_rut.", '".$dv."', '".$this->quitar_tildes($this->_direNom)."', '".$this->quitar_tildes($this->_direCalle)."', '".$this->quitar_tildes($this->_direCiudad)."', '".$this->quitar_tildes($this->_direRegion)."', '".$this->_nombre."', '".$this->_correo."', '".$this->_telefono."', '".$this->quitar_tildes($this->_direCalleF)."', '".$this->_neto."', '".$this->_rutEje."', '".$this->_usuEje."', '".substr($this->_rutDesc, 0, strlen($this->_rutDesc) - 1)."', 1, '".$this->_rs."', '".$this->_bruto."', '".$this->_descuentototal."', '".$this->_fechaReferencia."')";
       $resultado = $conect->insertRetornandoId($query);
        if ($resultado > 0){
             $listadoDetalle = $this->_xmlCotizacion->OrderDetail->ListOfItemDetail->ItemDetail;
                 for ($i=0; $i < count($listadoDetalle); $i++) { 
              
                   $this->_texto = $listadoDetalle[$i]->BaseItemDetail->ItemIdentifiers->PartNumbers->SellerPartNumber->PartIDExt;
                   $this->_codigo = $listadoDetalle[$i]->BaseItemDetail->ItemIdentifiers->PartNumbers->BuyerPartNumber->PartIDExt;
                   $this->_codigo = str_replace("Código: ", "", $this->_codigo);
                   $this->_detalle = $listadoDetalle[$i]->BaseItemDetail->ItemIdentifiers->ItemDescription;
                   $this->_cantidad = $listadoDetalle[$i]->BaseItemDetail->TotalQuantity->QuantityValue;
                   $this->_precio = $listadoDetalle[$i]->PricingDetail->ListOfPrice->Price->UnitPrice->UnitPriceValue;
                   $this->_moneda = $listadoDetalle[$i]->PricingDetail->ItemAllowancesOrCharges->AllowOrCharge->TypeOfAllowanceOrCharge->MonetaryValue->Currency->CurrencyCoded;
                   $this->_codigoChc = explode(")", $this->_detalle);
                   $this->_codigoChc = trim(str_replace("(", "", $this->_codigoChc[0]));
                   $this->_correlativo = $listadoDetalle[$i]->BaseItemDetail->LineItemNum->SellerLineItemNum;
                   $conect2 = new conexionMySql(); 
                   $query2 = "INSERT INTO detalle(idpedido, idtexto, idcodigo, descripcion, cantidad, precio, moneda, codigochc, correlativo) VALUES(".$resultado.", '".$this->quitar_tildes($this->_texto)."', '".$this->_codigo."', '".$this->quitar_tildes($this->_detalle)."', '".$this->_cantidad."', '".$this->_precio."', '".$this->_moneda."', '".$this->_codigoChc."', '".$this->_correlativo."')";
                   $resultado2 = $conect2->ejecuta($query2);

                   $netoCalculado = $netoCalculado + ($this->_precio*$this->_cantidad);
                }

                $query2 = "UPDATE pedido SET totalnetocalculado = '".$netoCalculado."' WHERE id =".$resultado."";
                $conect->ejecuta($query2);

                return $respuesta."-".$this->_usuEje;


       }else{
        $query = "INSERT INTO pedido_error(buyerordernumber, desde) VALUES('".$this->_ordenDeCompra."', 'Menual')";
       $resultado = $conect->insertRetornandoId($query);
        return "error al insertar cabecera de oc ".$this->_ordenDeCompra;

       }


   
 
}


private function codificaStringBD($string) 
    { 
  $stringLimpio = str_replace("'", "comillaSimple$$$", $string);
  $stringLimpio = str_replace('"', 'comillaDoble$$$', $stringLimpio);

  return $stringLimpio;
}


private function decodificaStringBD($string) 
    { 
  $stringLimpio = str_replace("comillaSimple$$$", "'", $string);
  $stringLimpio = str_replace('comillaDoble$$$', '"', $stringLimpio);

  return $stringLimpio;
}


private function _traerOCahoraConBd() 
    { 
$addr = "www.mercadopublico.cl/wsoc/wsGetOc.asmx?WSDL";
$usr = "966708409";
$pass = "chilecompra523";
$meth = "GenerateXMLOCbyCode";
 require_once('../../webServices/nusoap/nusoap.php');
        $client = new nusoap_client("http://$addr", true);

        $error = $client->getError();
        if ($error) {
            return "Constructor error".$error;
        }
        $client->setCredentials($usr, $pass, "basic");
        $result = "";

        if ($client) {
            $result = $client->call($meth, array("porCode" => $this->_ordenDeCompra));
        }
        if ($client->fault) {
            return "Fault";
            print_r($result);
        }
        else {
            $error = $client->getError();
            if ($error){
                return "Error after call: ".$error;
            }
            else{
                if (strpos($result["GenerateXMLOCbyCodeResult"],'Se ha producido un error') !== false) {
                    return 'no_existe';
                }else{
                    $xml = simplexml_load_string(utf8_encode($result["GenerateXMLOCbyCodeResult"]));
                    $listadoCotizaciones = $xml->OrdersList->Order;
                    $this->_xmlCotizacion = $listadoCotizaciones[0];
                    return $this->_guardarCotizacionDesdePortal();
               
                }
               
                 
            }
        }
 
}


private function _guardarRobotCompetenciaConBd()
    { 
  
    require_once "../clases/controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
          $query = "INSERT INTO robotcompetencia(ejecutado, cotizacionesleidas, tiempoejecucion) VALUES(CURRENT_TIMESTAMP, ".$this->_leidas.", '".$this->_tiempo."') ";
         $conect->ejecuta($query);
  
  
}


private function _guardarRobotConBd() 
    { 
  
    require_once "../clases/controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
          $query = "INSERT INTO robot(ejecutado, cotizacionesleidas, cotizacionessinasignar, cotizacionesyahechas, cotizacionesnoaplica, textoerror, cotizacionesasignadas, cotizacionesyahechasntavta, totalplata, tiempoejecucion, tipo) VALUES(CURRENT_TIMESTAMP, ".$this->_leidas.", ".$this->_sinAsignar.", ".$this->_yaHecha.", ".$this->_noAplica.", '".$this->_error."', ".$this->_asignada.", ".$this->_yaHechaNtaVta.", ".$this->_totalplata.", '".$this->_tiempo."', '".$this->_tipo."') ";
         $conect->ejecuta($query);
  
  
}


private function _guardarLicitacionConBd() 
    { 
  
    require_once "clases/controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
          $query = "INSERT INTO licitacion(numeroadquisicion, tipoadquisicion, nombreadquisicion, descripcion, organismo, regioncompradora, fechapublicacion, fechacierre, descripciondelproducto, codigoonu, unidadmedida, cantidad, generico, nivel1, nivel2, nivel3) VALUES('".$this->_numero."', '".$this->_tipo."', '".$this->_nombre."', '".$this->_descripcion."', '".$this->_organizacion."', '".$this->_region."', '".$this->_fpubli."', '".$this->_fcierre."', '".$this->_desPro."', ".$this->_onu.", '".$this->_unidad."', ".$this->_cantidad.", '".$this->_generico."', '".$this->_n1."', '".$this->_n2."', '".$this->_n3."') ";
         $asdf = $conect->insertRetornandoId($query);

         return $asdf;
 
  
}

private function _buscarOCtelemarketingConBd() 
    { 
  ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

 require "../controlador/conexionOracle.php"; 
    $conect2 = new conexionOracle();
    $query = "SELECT numord, fecord, facnom, rutcli, cencos, facdir, observ, tipweb, descli, ORDWEB, NUMNVT FROM We_notavta WHERE numord= ".$this->_numero."";
    $resultado = $conect2->selectSimple($query);
 
       $objetoTemporal = new stdClass();
     if (count($resultado)==0){
        $objetoTemporal->resultado = "noExiste";
     }else{
        $dato = $resultado[0];
        $objetoTemporal->resultado = "existe";
        $objetoTemporal->fecord = $dato["FECORD"];
        $objetoTemporal->rutcli = $dato["RUTCLI"];
        $objetoTemporal->cencos = $dato["CENCOS"];
        $objetoTemporal->descli = $dato["DESCLI"];
        $objetoTemporal->facnom = $dato["FACNOM"];
        if (strlen(trim($dato["NUMNVT"]))==0){
            $objetoTemporal->estado = "borrable";
        }else{
            $objetoTemporal->estado = "bloqueado";
        }
        
        //  $objetoTemporal->ejeX = array("0"=>$aplicacion->getAnio());
     } 

 
         
         $json = json_encode($objetoTemporal);
             return $json;
 
  
}


private function _borrarOCtelemarketingConBd() 
    { 
   ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

 $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
    $query = "UPDATE We_notavta SET NUMNVT=999 WHERE numord= ".$this->_numero."";
 
              $compiled = oci_parse($conn, $query);
 
        oci_execute($compiled);
 
    return $compiled;
 
}

private function _estadoClienteConBd() 
    { 
  
 
 
    if (trim($this->_cencos)=="vacio"){

        require_once "../controlador/conexionOracle.php"; 
      $conectO = new conexionOracle(); 
        $query2 = "SELECT CENCOS AS PRINCIPAL FROM EN_CLIENTE WHERE RUTCLI = ".$this->_rutCli." AND CODEMP=3";
        $resultado = $conectO->selectSimple($query2);
        $asdf = $resultado[0];

      $asdf['PRINCIPAL'];

        $conectO3 = new conexionOracle(); 
          $query3 = "SELECT getestadocliente(3, ".$this->_rutCli.", ".$asdf['PRINCIPAL'].") AS ESTADO FROM EN_CLIENTE WHERE RUTCLI = ".$this->_rutCli." AND CODEMP=3";
         $resultado3 = $conectO3->selectSimple($query3);
        $asdf3 = $resultado3[0];
        return $asdf3['ESTADO'];

      }else{

        require_once "../controlador/conexionOracle.php"; 
      $conectO = new conexionOracle(); 
        $query2 = "SELECT getestadocliente(3, ".$this->_rutCli.", ".$this->_cencos.") AS ESTADO FROM EN_CLIENTE WHERE RUTCLI = ".$this->_rutCli." AND CODEMP=3";
        $resultado = $conectO->selectSimple($query2);
        $asdf = $resultado[0];
        return $asdf['ESTADO'];
      }

 
 
}


private function _procesarOCConBd() 
    { 
  
    require_once "clases/controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
    if (strlen(trim($this->_rutEje))<6){
        $estado = 1;
      }else{
        $estado = 2;
      }


require_once "clases/controlador/conexionOracle.php"; 
  $conectO = new conexionOracle(); 
    $query2 = "SELECT * FROM EN_NOTAVTA WHERE trim(docaso)= '".$this->_ordenDeCompra."'";
    $resultado = $conectO->selectSimple($query2);

if (count($resultado[0])!=0){
$estado = 8;
}


          $query = "UPDATE pedido SET idestado =".$estado.", rutdescripcion='".substr ($this->_rutDesc, 0, strlen($this->_rutDesc) - 1)."', rutejecutivo='".$this->_rutEje."', usuarioejecutivo='".$this->_usuEje."', razonsocial='".$this->_rs."', fechahoraaceptada = CURRENT_TIMESTAMP WHERE id= ".$this->_idPedido."";
         $conect->ejecuta($query);
  
  return $estado;
 
}


  // SELECT id, fechahoracreacion  FROM `pedido` WHERE YEAR(fechahoracreacion)= 2015 AND MONTH(fechahoracreacion)= 8 AND DAY(fechahoracreacion)< 21

private function _generarEstadisticaOCConBd() 
    { 
 
        $respuesta = array();
    $datediff = strtotime($this->_hasta) - strtotime($this->_desde);
    $dierencia = $datediff/(60*60*24);
     if (($dierencia==0) or (strlen(trim($this->_hasta))==0)) {
        $trozos = explode("-", $this->_desde);
         require_once "../controlador/conexionMySql.php"; 
    
    $conect = new conexionMySql(); 
 
          $query = "SELECT id, buyerordernumber, fechahoracreacion FROM pedido WHERE idestado=4 AND YEAR(fechahoracreacion)= ".$trozos[2]." AND MONTH(fechahoracreacion)= ".$trozos[1]." AND DAY(fechahoracreacion)= ".$trozos[0]."";
          $respuesta =  $conect->selectMultiple($query);
    }else{
        for ($i=0; $i <= $dierencia; $i++) { 
            $fecha = date('d-m-Y', strtotime($this->_desde. ' + '.$i.' days'));
            $trozos = explode("-", $fecha);
            require_once "../controlador/conexionMySql.php"; 
    
    $conect = new conexionMySql(); 
 
          $query = "SELECT id, buyerordernumber, fechahoracreacion FROM pedido WHERE idestado=4 AND YEAR(fechahoracreacion)= ".$trozos[2]." AND MONTH(fechahoracreacion)= ".$trozos[1]." AND DAY(fechahoracreacion)= ".$trozos[0]."";
           $resultado =  $conect->selectMultiple($query);
          $respuesta = array_merge($respuesta, $resultado);
        }

    }
    $existenTlmk = 0;
    $existentvta = 0;
    require_once "../controlador/conexionOracle.php"; 
    for ($i=0; $i <count($respuesta) ; $i++) { 
        $fila = $respuesta[$i];

     

            $conectO2 = new conexionOracle(); 
            $query2 = "SELECT * FROM We_notavta WHERE tipweb=4 AND trim(FACNOM)= '".trim($fila[1])."'";
            $resultado2 = $conectO2->selectSimple($query2);

        if (count($resultado2[0])!=0){
            $existenTlmk++;
            $filaOracle = $resultado2[0];
            if (strlen(trim($filaOracle["NUMNVT"]))>0) {
                $existentvta++;
            }

             
        }
    }
 
      return "TOTAL: ".count($respuesta).". En telemarketing: ".$existenTlmk.", en nota de venta: ".$existentvta;
}




private function _borrarOCConBd() 
    { 
  
    require_once "clases/controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
          $query = "DELETE FROM detalle WHERE idpedido=".$this->_idPedido."";
          $conect->ejecuta($query);

          $conect2 = new conexionMySql(); 
          $query2 = "DELETE FROM pedido WHERE id=".$this->_idPedido."";
          $conect2->ejecuta($query2);
  
 
}



private function _listadoSinProcesarConBd() 
    { 
  
    require_once "clases/controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
          $query = "SELECT * FROM pedido WHERE idestado=5";
 
 
        $resultado = $conect->selectMultiple($query);
        
        return $resultado;
 
}



private function _guardarNuevaRelacionOCConBd() 
    { 
  
    require_once "../controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
  
          $query = "UPDATE detalle SET idcodigo='".$this->_idDimerc."', unidad='".$this->_unidad."', fechahoracambiorelacion=CURRENT_TIMESTAMP, codigoanterior=idcodigo WHERE id = ".$this->_idDetalle." ";
 
        $resultado = $conect->ejecuta($query);
 
 
        return $resultado;
     
}


private function _reemplazarProductoConBd() 
    { 
  
    require_once "../controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 

    switch ($this->_tipo) {
        case 'sugerencia':
             $query = "UPDATE detalle SET idcodigo='".$this->_idDimerc."', fechahorareemplazo=CURRENT_TIMESTAMP, codigoanterioralreemplazo='".$this->_idDimercOld."', unidad='".$this->_unidad."', tiporeemplazo='Ejecutivo acepto sugerencia' WHERE id = ".$this->_idDetalle." ";
            break;
        case 'sinSug':
             $query = "UPDATE detalle SET idcodigo='".$this->_idDimerc."', fechahorareemplazo=CURRENT_TIMESTAMP, codigoanterioralreemplazo='".$this->_idDimercOld."', unidad='".$this->_unidad."', tiporeemplazo='No existia sugerencia para el producto' WHERE id = ".$this->_idDetalle." ";
            break;
        case 'sugSinStock':
             $query = "UPDATE detalle SET idcodigo='".$this->_idDimerc."', fechahorareemplazo=CURRENT_TIMESTAMP, codigoanterioralreemplazo='".$this->_idDimercOld."', unidad='".$this->_unidad."', tiporeemplazo='La sugerencia tenia stock insuficiente' WHERE id = ".$this->_idDetalle." ";
            break;
        case 'sugConStock':
             $query = "UPDATE detalle SET idcodigo='".$this->_idDimerc."', fechahorareemplazo=CURRENT_TIMESTAMP, codigoanterioralreemplazo='".$this->_idDimercOld."', unidad='".$this->_unidad."', tiporeemplazo='Habia sugerencia con stock, pero aun asi el ejecutivo opto por elegir un producto del listado' WHERE id = ".$this->_idDetalle." ";
            break;
        case 'listado':
             $query = "UPDATE detalle SET idcodigo='".$this->_idDimerc."', fechahorareemplazo=CURRENT_TIMESTAMP, codigoanterioralreemplazo='".$this->_idDimercOld."', unidad='".$this->_unidad."', descripcion='(reemplazado)".$this->_nuevoTexto."', codigochc='reemplazado', tiporeemplazo='El ejecutivo eligio un producto del listado' WHERE id = ".$this->_idDetalle." ";
            break;
    }
  
         
        $resultado = $conect->ejecuta($query);
 
 
        return $resultado;
     
}




private function _accionRechazoConBd() 
    { 
  
    require_once "../controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
     switch ($this->_tipo) {
       case 1:
 
          $query = "UPDATE pedido SET idestado=2 WHERE id = ".$this->_idPedido." ";
 
        $resultado = $conect->ejecuta($query);
        return $resultado;
         break;
       case 2:
         return $this->_aceptarOrdenConBd();
         break;
       case 3:
        $query = "UPDATE pedido SET idestado=6 WHERE id = ".$this->_idPedido." ";
 
        $resultado = $conect->ejecuta($query);
        
        return $resultado;
         break;
     }
}


private function _cambiarAsignacionConBd() 
    { 
  
    require_once "../controlador/conexionMySql.php"; 
 
    $conect = new conexionMySql(); 
 
     switch ($this->_tipo) {
       case 1:
        $ejecutivo = $this->dameEjecutivoDesdePortal(trim($this->_rutCli));
        $ejecutivo = $ejecutivo[0];
        $rutEjecutivo=$ejecutivo["RUTVENDEDOR"];
        $usuarioEjecutivo=$ejecutivo["USUARIO"];
        $this->_rs =  $this->dameRsDesdePortal(trim($this->_rutCli));
 
        if (strlen(trim($rutEjecutivo))<6){
          $query = "UPDATE pedido SET idestado=1, rutdescripcion='Descartado', fechahorareasignacion=CURRENT_TIMESTAMP, razonsocial='".$this->_rs."' WHERE id = ".$this->_idPedido." ";
        }else{
          $query = "UPDATE pedido SET idestado=2, rutdescripcion='Descartado', fechahorareasignacion=CURRENT_TIMESTAMP, rutejecutivo='".$rutEjecutivo."', usuarioejecutivo='".$usuarioEjecutivo."', razonsocial='".$this->_rs."' WHERE id = ".$this->_idPedido." ";
        }

        $resultado = $conect->ejecuta($query);
        return $resultado;
         break;
       case 2:
        $ejecutivo = $this->dameEjecutivoDesdePortal(trim($this->_rutCli));
        $ejecutivo = $ejecutivo[0];
        $rutEjecutivo=$ejecutivo["RUTVENDEDOR"];
        $usuarioEjecutivo=$ejecutivo["USUARIO"];
        $this->_rs =  $this->dameRsDesdePortal(trim($this->_rutCli));
        if (strlen(trim($rutEjecutivo))<6){
          $query = "UPDATE pedido SET idestado=1, rutdescripcion='Descartado', fechahorareasignacion=CURRENT_TIMESTAMP, razonsocial='".$this->_rs."' WHERE trim(rutdescripcion) = '".trim($this->_rutDesc)."' and rut = ".$this->_rutCli."";
        }else{
          $query = "UPDATE pedido SET idestado=2, rutdescripcion='Descartado', fechahorareasignacion=CURRENT_TIMESTAMP, rutejecutivo='".$rutEjecutivo."', usuarioejecutivo='".$usuarioEjecutivo."', razonsocial='".$this->_rs."' WHERE trim(rutdescripcion) = '".trim($this->_rutDesc)."' and rut = ".$this->_rutCli."";
        }

        $resultado = $conect->ejecuta($query);
        return $resultado;
         break;
       case 3:
        $ejecutivo = $this->dameEjecutivoDesdePortal(trim($this->_rutCli));
        $ejecutivo = $ejecutivo[0];
        $rutEjecutivo=$ejecutivo["RUTVENDEDOR"];
        $usuarioEjecutivo=$ejecutivo["USUARIO"];
        $this->_rs =  $this->dameRsDesdePortal(trim($this->_rutCli));
        if (strlen(trim($rutEjecutivo))<6){
          $query = "UPDATE pedido SET idestado=1, rutdescripcion='Descartado', fechahorareasignacion=CURRENT_TIMESTAMP, razonsocial='".$this->_rs."' WHERE trim(rutdescripcion) = '".trim($this->_rutDesc)."' and rut = ".$this->_rutCli."";
        }else{
          $query = "UPDATE pedido SET idestado=2, rutdescripcion='Descartado', fechahorareasignacion=CURRENT_TIMESTAMP, rutejecutivo='".$rutEjecutivo."', usuarioejecutivo='".$usuarioEjecutivo."', razonsocial='".$this->_rs."' WHERE trim(rutdescripcion) = '".trim($this->_rutDesc)."'";
        }
          $resultado = $conect->ejecuta($query);

         $conect2 = new conexionMySql(); 
         $query2 = "INSERT INTO error(numero) VALUES(".trim($this->_rutDesc).")";
         $resultado2 = $conect2->ejecuta($query2);
        
        return $resultado;
         break;
     }
}


private function _listadoCodigoConBd() 
    { 
  
    require_once "../controlador/conexionOracle.php"; 
 
    $conect = new conexionOracle(); 
 
    $query = "SELECT * FROM gob_relacion order by idtipo";
 
    $resultado = $conect->selectSimple($query);
    return $resultado;
}


private function _borrarRelacionDeshabilitarConBd() 
    { 
  
    require_once "../controlador/conexionMySql.php"; 
    $conect2 = new conexionMySql(); 
 
    $query2 = "UPDATE configuracion set habilitarenvio = 'no' where id=1";
 
    $resultado2 = $conect2->ejecuta($query2);


     $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
     $query = "DELETE from gob_relacion";
     $compiled = oci_parse($conn, $query);
 
        oci_execute($compiled);

  
  
}


private function _habilitarEnvioConBd() 
    { 
  
    require_once "../controlador/conexionMySql.php"; 
    $conect2 = new conexionMySql(); 
 
    $query2 = "UPDATE configuracion set habilitarenvio = 'si' where id=1";
 
    $resultado2 = $conect2->ejecuta($query2);
 
}

private function _tipoWsConBd() 
    { 
  
    require_once "../clases/controlador/conexionMySql.php"; 

    $conect = new conexionMySql(); 
 
    $query = "SELECT fechaultimowsmasivo, CURRENT_DATE FROM configuracion WHERE id=1";
 
    $resultado = $conect->selectSimple($query);


    if ($resultado[0]==$resultado[1]){
        return "diario";

    }else{
        $conect2 = new conexionMySql();
        $query2 = "UPDATE configuracion set fechaultimowsmasivo = CURRENT_DATE where id=1";
        $resultado2 = $conect2->ejecuta($query2);
        return "masivo";
    }


    
 
}




private function _detalleOCConBd() 
    { 
session_name("robotChilecompra");
@session_start();  
    require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
$arrayPedido = array();
 
    $query = "SELECT * from pedido  WHERE id = ".$this->_idPedido." ";
 
 
    $resultado = $conect->selectSimple($query);
    $arrayPedido[0]=$resultado;
   $conect2 = new conexionMySql(); 
   $query2 = "SELECT * from detalle  WHERE idpedido = ".$this->_idPedido." ";
   $resultado2 = $conect2->selectMultiple($query2);
   $arrayPedido[1]=$resultado2;

    return $arrayPedido;

    
}




private function _validaLogInConBd() 
    { 
 

if ($this->_rut=="15341801"){





}

    require_once "../controlador/conexionMySql.php"; 
     require "../controlador/conexionOracle.php"; 
    $conect2 = new conexionOracle();
    $query = "select clave1 from RE_CLAVSIS where rutusu= ".$this->_rut."";
    $resultado = $conect2->selectSimple($query);
    $arr = $resultado[0];

    if (strlen($arr["CLAVE1"])>0){
         
        if(mb_strtoupper(trim($arr["CLAVE1"]),'utf-8') == mb_strtoupper(trim($this->_pass),'utf-8')){ 

              $conect3 = new conexionOracle();
             $query = "select * from MA_USUARIO where rutusu= ".$this->_rut."";
             $resultado2 = $conect3->selectSimple($query);
             $dato = $resultado2[0];

             $conect4 = new conexionMySql();
             $query4 = "select * from reemplazo where rutreemplazante= ".trim($this->_rut)." AND hasta IS NULL";
             $resultado4 = $conect4->selectMultiple($query4);
           
              session_name("robotChilecompra");
              session_start(); 
              $_SESSION['rut_usu']=$this->_rut;
              $_SESSION['nombre']=$dato["NOMBRE"];
              $_SESSION['apellido']=$dato["APEPAT"];
              $_SESSION['correo']=$dato["MAIL01"];
              $_SESSION["autentificado"]= "si";
              $_SESSION["iniciado"]= date("Y-n-j H:i:s");
              $_SESSION["reemplazo"]= $resultado4;
              
              if (($this->_rut=="15341801") or ($this->_rut=="15087797") or ($this->_rut=="22087581")){
                // $this->_revisarRobotConBd();
                // if($this->_revisarDolarConBd()){
                //   $_SESSION["dolar"]= "ok";
                // }else{
                //   $_SESSION["dolar"]= "error";
                // }
                
                $_SESSION["admin"]= "si";
                return "go*admin";
              }else{
                 // $this->_revisarRobotConBd();
                 // $test = $this->_revisarDolarConBd();
                 $_SESSION["admin"]= "no";
                return "go*usuario";
              }
 
        } else{
        return "La Contraseña Que Usted Ha Escrito Es Incorrecta"; 
        }
    }else{
        return "Usted no tiene asignada ninguna clave en telemarketing."; 
    }     
       
}


private function _revisarDolarConBd() 
    { 
          $meses = array("Enero"=>1, "Febrero"=>2, "Marzo"=>3, "Abril"=>4, "Mayo"=>5, "Junio"=>6, "Julio"=>7, "Agosto"=>8, "Septiembre"=>9, "Octubre"=>10, "Noviembre"=>11, "Diciembre"=>12);

            require_once "../controlador/conexionMySql.php"; 
                $conect = new conexionMySql(); 
                $queryD = "select fechaultimodolarrevisado from configuracion where fechaultimodolarrevisado = CURRENT_DATE and id=1";
                $resultado3 = $conect->selectSimple($queryD);
                if ($resultado3=="LaConsultaNoTieneRegistros"){
                  include_once("../librerias/snoopy.class.php");
                  include_once("../librerias/htmlsql.class.php");

                  $wsql = new htmlsql();
                  $url = "http://si3.bcentral.cl/Indicadoressiete/secure/Serie.aspx?gcode=PRE_TCO&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAdwA1ADQAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
                  
                  if (!$wsql->connect('url', $url)){
                    print 'Error while connecting: ' . $wsql->error;
                    exit;
                  }

                  if (!$wsql->query('SELECT id, text FROM span WHERE $class == "obs"')){
                  print "Query error: " . $wsql->error; 
                  exit;
                  }

                  $conect2 = new conexionMySql(); 
                  $query2 = "delete from dolar where fecha > CURRENT_DATE - 10";
                  $resultado2 = $conect2->ejecuta($query2);

                  $listado=$wsql->fetch_objects();
                  $fecha = date('Y-m-j');
                  $nuevafecha = strtotime ( '-10 day' , strtotime ( $fecha ) ) ;
                  $nuevafecha = date ( 'Y-m-j' , $nuevafecha );

                   for ($i=0; $i < count($listado); $i++) { 
                   $id = $listado[$i]->id;
                   $fecha = explode("gr_ctl", $id);
                   $trozo = explode("_", $fecha[1]);
                   $mes = array_search($trozo[1], array_keys($meses))+1;
                   $dia = ($trozo[0]-1);
                   if ((strtotime($nuevafecha)<strtotime("2015-".$mes."-".$dia)) and (strtotime(date('Y-m-j'))>=strtotime("2015-".$mes."-".$dia))){
                      $fechaInsertar = "2015-".$mes."-".$dia;
                    if (checkdate($mes, $dia, 2015)){
                        if (strlen($listado[$i]->text)>1){
                        $conect3 = new conexionMySql(); 
                        $query3 = "INSERT INTO dolar(fecha, valor) VALUES('".$fechaInsertar."', '".str_replace(",",".",$listado[$i]->text)."')";
                        $resultado3 = $conect3->ejecuta($query3);
                      }else{
                        $conect3 = new conexionMySql(); 
                        $query3 = "INSERT INTO dolar(fecha) VALUES('".$fechaInsertar."')";
                        $resultado3 = $conect3->ejecuta($query3);
                      }


                    }
                    

                    
                   }


                  
                }

                 $conect4 = new conexionMySql(); 
                $query4 = "UPDATE configuracion SET fechaultimodolarrevisado = CURRENT_DATE where id=1";
                $resultado4 = $conect4->ejecuta($query4);
                return true;
              }else{
                return true;
              } 
 }



private function _revisarRobotConBd() 
    { 
 date_default_timezone_set('America/Santiago');
            require_once "../controlador/conexionMySql.php"; 
                $conect = new conexionMySql(); 
                $queryD = "select ejecutado, CURRENT_TIMESTAMP, TIMESTAMPDIFF(SECOND, CURRENT_TIMESTAMP, ejecutado), TIMESTAMPDIFF(MINUTE, CURRENT_TIMESTAMP, ejecutado)  from robot WHERE tipo = 'diario' order by ejecutado desc limit 1";
                $resultado3 = $conect->selectSimple($queryD);
                $trozo = explode(" ", $resultado3[0]);
                $trozo2 = explode(" ", $resultado3[1]);
                $horaActual = $trozo2[1];
                $horaActual = explode(".", $horaActual);

                if((strtotime(date("18:00:00"))>strtotime($horaActual[0])) and (strtotime(date("11:00:00"))<strtotime($horaActual[0]))) {
                     if (abs($resultado3[3]) >90){
                        return $this->notificaError("ejecucionDiario", $resultado3[3], $resultado3[0], "");
                    }
                    
                } 

                $conect = new conexionMySql(); 
                $queryD = "select ejecutado, CURRENT_TIMESTAMP, TIMESTAMPDIFF(SECOND, CURRENT_TIMESTAMP, ejecutado), TIMESTAMPDIFF(MINUTE, CURRENT_TIMESTAMP, ejecutado)  from robot WHERE tipo = 'masivo' order by ejecutado desc limit 1";
                $resultado3 = $conect->selectSimple($queryD);
                $trozo = explode(" ", $resultado3[0]);
                $trozo2 = explode(" ", $resultado3[1]);
                $horaActual = $trozo2[1];
                $horaActual = explode(".", $horaActual);

                if((strtotime(date("17:00:00"))>strtotime($horaActual[0])) and (strtotime(date("11:00:00"))<strtotime($horaActual[0]))) {
                     if (abs($resultado3[3]) >180){
                        return $this->notificaError("ejecucionMasivo", $resultado3[3], $resultado3[0], "");
                    }
                    
                }                 
       
 }


private function notificaError($tipo, $dif, $ult, $error)
{
 
switch ($tipo) {
    case 'ejecucionDiario':
        $dif = explode(".", $dif);
        $ult = explode(".", $ult);
        $mensaje = "Por alguna razon, el robot que ejecuta el web services diario de chilecompra ha dejado de funcionar.";
        $mensaje .= "<br><br>Su ultima ejecucion fue:";
        $mensaje .= "<br>".$ult[0].", hace ".abs($dif[0])." minutos";
        $mensaje .= "<br><br><br>Por favor, hacer las gestiones para resolver el problema.<br>Gracias!";
        $asunto = "Error Robot Gobierno";
        break;
    case 'ejecucionMasivo':
        $dif = explode(".", $dif);
        $ult = explode(".", $ult);
        $mensaje = "Por alguna razon, el robot que ejecuta el web services masivo de chilecompra ha dejado de funcionar.";
        $mensaje .= "<br><br>Su ultima ejecucion fue:";
        $mensaje .= "<br>".$ult[0].", hace ".abs($dif[0])." minutos";
        $mensaje .= "<br><br><br>Por favor, hacer las gestiones para resolver el problema.<br>Gracias!";
        $asunto = "Error Robot Gobierno";
        break;
}
 
  $respuestaCorreo = $this->_enviaCorreoPorCuentaNueva("chat@dimerc.cl", "ednunez@dimerc.cl", "Eduardo", $asunto, $mensaje, $mensajeSinHtml);


}


private function dameSegundosDeHora($hora)
{
 
$sinpunto = explode(".", $hora);
$trozo = explode(":", $sinpunto[0]);
$trozo2 = explode(" ", $trozo[0]);
 
if (count($trozo2)>2){
    return 99999;
}

return (abs($trozo2[count($trozo2)-1])*240)+(abs($trozo[1])*60)+abs($trozo[2]);


}


    private function _enviaCorreoPorCuentaNueva($emisor, $destinatario, $nomDestinatario, $asunto, $mensaje, $mensajeSinHtml)
    { 
 
 
        require_once '../librerias/PHPMailer-master/class.phpmailer.php';
        require_once('../librerias/PHPMailer-master/class.pop3.php'); 

        $pop = new POP3();
        $pop->Authorise('190.196.13.157', 110, 30, 'dimerc', 'J0c9qgXhHAUCQM77', 1);

        $mail = new PHPMailer;
         $mail->IsSMTP();                                      // Set mailer to use SMTP
        $mail->Host = '190.196.13.157';                 // Specify main and backup server
        $mail->Port = 2323;                                    // Set the SMTP port
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'dimerc';                // SMTP username
        $mail->Password = 'J0c9qgXhHAUCQM77';                  // Enable encryption, 'ssl' also accepted

        $mail->From = 'portalgobierno@dimerc.cl';
        $mail->FromName = 'Portal Gobierno';
        $mail->AddAddress($destinatario, $nomDestinatario);  // Add a recipient
        $mail->AddAddress($destinatario);               // Name is optional

        $mail->IsHTML(true);                                  // Set email format to HTML

        $mail->Subject = $asunto;
        $mail->Body    = $mensaje;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if(!$mail->Send()) {
           return 'ERROR';
           // echo 'Mailer Error: ' . $mail->ErrorInfo;
           exit;
        }

        return 'ENVIADO';
 
    } 


private function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
 

private function _aceptarOrdenConBd() 
    { 

$error = "no";
$errorRelacionDetalle = "";




 require_once "../controlador/conexionMySql.php"; 
  
  $conect = new conexionMySql(); 
  $query = "SELECT habilitarenvio FROM configuracion WHERE id=1";
  $resultado = $conect->selectSimple($query);


if ($resultado["habilitarenvio"]=="no") {
    return "error_deshabilitado->";
    exit();
}




 $array = $this->_detalleOCConBd();
$oc= $array[0];
 
$detalle=$array[1];
$detalleTemporal = array();
$codigoDuplicado = array();
$codigoDuplicadoEncontrado = array();
$filaDuplicadoEncontrado = array();
$mayorDuplicado = 0;
require_once "../controlador/conexionOracle.php"; 
 require_once "../controlador/conexionMySql.php"; 


  $conect = new conexionMySql(); 
  $query = "SELECT trim(idcodigo), count(idcodigo) FROM detalle WHERE idpedido = ".$oc[0]." AND LENGTH(idcodigo) > 1 group by  idcodigo order by count(idcodigo) desc";
  $resultado = $conect->selectMultiple($query);


  if ($resultado!="LaConsultaNoTieneRegistros"){
        $filaPrimera = $resultado[0];
        $mayorDuplicado = $filaPrimera[1];
        for ($d=0; $d < count($resultado); $d++) { 
            $filaDupicado = $resultado[$d];
            if ($filaDupicado[1]>1){
            $duplicarOC = "si";
            array_push($codigoDuplicado, $filaDupicado);
            } 
        }
  }

 

  if (count($codigoDuplicado)>0){
 

                 for ($k=0; $k < count($detalle); $k++) { 
                   $filaDetalle = $detalle[$k];
                    $encontrado = "no";
                    $seGuarda = "si";
                        for ($d=0; $d < count($codigoDuplicado); $d++) { 
                           $filaDupicado = $codigoDuplicado[$d];
                          
                                 if (trim($filaDetalle[3])==trim($filaDupicado[0])){
                                     if (in_array(trim($filaDetalle[3]), $codigoDuplicadoEncontrado)) {
                                        $seGuarda ="no";
                                        array_push($filaDuplicadoEncontrado, $filaDetalle);
                                    } else{
                                        array_push($codigoDuplicadoEncontrado, trim($filaDetalle[3]));
                                         
                                    }
                                }
 
                        }
                        if ($seGuarda == "si"){
                            array_push($detalleTemporal, $filaDetalle);
                        }
  
                 }
 $detalle = $detalleTemporal;
  } 
 
if (strpos($_SERVER['REQUEST_URI'],'Desarrollo') !== false) {
    //Estoy en Desarrollo
 }else{
    //Estoy en Productivo
         $conectO = new conexionOracle(); 
            $query = "SELECT * FROM EN_NOTAVTA WHERE trim(docaso)= '".$oc[2]."'";
            $resultado = $conectO->selectSimple($query);

        if (count($resultado[0])!=0){
            $tipoError = "ya existe nota de venta";
           $conect2 = new conexionMySql(); 
          $query2 = "UPDATE pedido SET idestado=8, tipoerror='".$tipoError."', fechahoraerror=CURRENT_TIMESTAMP, cencos = '".$this->_cencos."' WHERE id=".$oc[0]."";
         $conect2->ejecuta($query2);
          return "existe_venta";
              exit();
        }

         $conectO2 = new conexionOracle(); 
            $query2 = "SELECT * FROM We_notavta WHERE trim(FACNOM)= '".$oc[2]."' AND rutCli<>22087581";
            $resultado2 = $conectO2->selectSimple($query2);

        if (count($resultado2[0])!=0){
            $tipoError = "ya existe bandeja telemarketing";
           $conect2 = new conexionMySql(); 
          $query2 = "UPDATE pedido SET idestado=8, tipoerror='".$tipoError."', fechahoraerror=CURRENT_TIMESTAMP, cencos = '".$this->_cencos."' WHERE id=".$oc[0]."";
         $conect2->ejecuta($query2);
          return "existe_tlmk";
              exit();
        }
}




 

     $conect = new conexionOracle();
    $query = "Select dm_ventas.notaweb_seq.nextval as Serie from dual";
    $resultado = $conect->selectSimple($query);
    $arr = $resultado[0];
    $numOrden = $arr["SERIE"];
    $dirFact = substr($oc[14], 0, 30); 
        if ((trim($oc["rutdescripcion"]) == "No hay") or (trim($oc["rutdescripcion"]) == "Encontrado, pero es un error conocido") or (trim($oc["rutdescripcion"]) == "Descartado")) {
                    $rut = $oc["rut"];
                  }else{
                    $rut =  $oc["rutdescripcion"];
                  }

$dscto = abs($oc["descuento"])*100/$oc["totalneto"];
$dscto = round($dscto, 2);
 

$dscto = (float)$dscto;
//  if (($dscto >0) and ($dscto <1)){
//  $tipoError = "descuento entre 0 y 1";
//    $conect2 = new conexionMySql(); 
//   $query2 = "UPDATE pedido SET idestado=7, tipoerror='".$tipoError."', fechahoraerror=CURRENT_TIMESTAMP, cencos = '".$this->_cencos."' WHERE id=".$oc[0]."";
//  $conect2->ejecuta($query2);
//   return "errorDecimal";
//       exit();
// }


         $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
                  $query = "INSERT INTO We_notavta (numord, fecord, facnom, rutcli, cencos, facdir, observ, tipweb, descli, ORDWEB) VALUES(:nord, trunc(sysdate), :ordweb, :rutcli, :cc, '0', :observ, '4', :dscto, :idbd)";
            

              $compiled = oci_parse($conn, $query);

if (strpos($_SERVER['REQUEST_URI'],'Desarrollo') !== false) {
    //Estoy en Desarrollo
      $rut = '22087581';
      $this->_cencos =0;
 }else{
    //Estoy en Productivo
}



// return $oc[0]."___".$numOrden."___".$rut."___".$this->_cencos."___".$oc[2]."___".$oc[4]."___".$dscto;

if (strlen(trim($oc[4]))>500){
     $descri = substr(trim($oc[4]), 0, 475);
     $descri = $descri . " ...(truncado por robot)";
}else{
    $descri = trim($oc[4]);
}

 

oci_bind_by_name($compiled, ':idbd', $oc[0]);
oci_bind_by_name($compiled, ':nord', $numOrden);
oci_bind_by_name($compiled, ':rutcli', $rut);
oci_bind_by_name($compiled, ':cc', $this->_cencos);
 
oci_bind_by_name($compiled, ':ordweb', $oc[2]);
oci_bind_by_name($compiled, ':observ', $descri);
oci_bind_by_name($compiled, ':dscto', $dscto);
$resultado = oci_execute($compiled);

 
 
if ($resultado==1){
$netoCalculado = 0;
$totalFlete=0;
$detalleFlete='';
for ($i=0; $i < count($detalle); $i++) { 
  $filaDetalle = $detalle[$i];
 $netoCalculado = $netoCalculado + ($filaDetalle[5]*$filaDetalle[6]);
 
if (trim($filaDetalle[7])=="USD"){
  $filaDetalle[6] = $this->dolarApesos($filaDetalle[6], $oc[3]);
   $tipoError = "dolar";
      $error = "si";
      break;
}
 




   if (strlen(trim($filaDetalle[9]))>0){

        $precio = round($filaDetalle[6]/$filaDetalle[9]);
         $cantidad = round($filaDetalle[5]*$filaDetalle[9]);
         $codigo = strtoupper(trim($filaDetalle[3]));

         $conect3 = new conexionOracle(); 
         $query3 = "SELECT unidad, iddimerc, precio FROM gob_relacion where trim(codigo) = '".trim($filaDetalle[8])."'";
         $resultado3 = $conect3->selectSimple($query3);
         if (count($resultado3[0])==1){
            $dato = $resultado3[0];
            $totalFlete = $totalFlete + (($filaDetalle[6]-$dato["PRECIO"])*$filaDetalle[5]);
            $detalleFlete.= $filaDetalle[8].": ".($filaDetalle[6]-$dato["PRECIO"])."_$$$_";
         }

         // else{
         //     $tipoError = "error relacion codigo dimerc: ".trim($filaDetalle[3])." y codigo chc: ".trim($filaDetalle[8]);
         //      $error = "si";
         //      $errorRelacion = "si";
         //      $errorRelacionDetalle.= $filaDetalle[0]."///123".trim($filaDetalle[3])."///123".trim($filaDetalle[8])."_123_";
         //      $precio = 0;
         //      $cantidad = 0;
         //      $codigo = 0;
         // }
   }else{

      $conect3 = new conexionOracle(); 
       $query3 = "SELECT unidad, iddimerc, precio FROM gob_relacion where trim(codigo) = '".trim($filaDetalle[8])."'";
       $resultado3 = $conect3->selectSimple($query3);

       if (count($resultado3)>1){
      //   $tipoError = "duplicidad codigo chc: ".trim($filaDetalle[8]);
      // $error = "si";
      // $precio = 0;
      // $cantidad = 0;
       break;
     }
    if (count($resultado3[0])==0){
        $tipoError = "error relacion codigo dimerc: ".trim($filaDetalle[3])." y codigo chc: ".trim($filaDetalle[8]);
      $error = "si";
      $errorRelacion = "si";
      $errorRelacionDetalle.= $filaDetalle[0]."///123".trim($filaDetalle[3])."///123".trim($filaDetalle[8])."_123_";
      $precio = 0;
      $cantidad = 0;
      $codigo = 0;
      }else{
        $dato = $resultado3[0];
         $precio = round($filaDetalle[6]/$dato["UNIDAD"]);
         $cantidad = round($filaDetalle[5]*$dato["UNIDAD"]);
         $codigo = $dato["IDDIMERC"];
         $totalFlete = $totalFlete + (($filaDetalle[6]-$dato["PRECIO"])*$filaDetalle[5]);
         $detalleFlete.= $filaDetalle[8].": ".($filaDetalle[6]-$dato["PRECIO"])."_$$$_";
     } 

 
   }

$codigo = strtoupper(trim($codigo));
session_name("robotChilecompra");
   // if (trim($_SESSION['rut_usu'])=="15723411"){
        if ($codigo!==0){
 
                   $conect4 = new conexionOracle(); 
                        $sql = "select a.codpro, a.despro, getfamilia(a.codpro) FAMILIA, b.stocks, b.stkcom , b.stocks- b.stkcom disponible  
                        from ma_product a, re_bodprod_dimerc b
                        where a.codpro = b.codpro
                        and b.codemp= 3 
                        and b.codbod = 1
                        and a.codpro = '".trim($codigo)."'";
                    
                       $resultado4 = $conect4->selectSimple($sql);
                      if (count($resultado4)==0){
                      // $tipoError = "error no existe stock para codigo dimerc: ".trim($codigo);
                      // $error = "si";
                      // $errorStock = "si";
                      // $errorStockDetalle.= $filaDetalle[0]."///123".trim($filaDetalle[3])."///123".trim($filaDetalle[8])."///123".trim($cantidad)."///123".trim($dato["STOCK"])."_123_";
                      }else{

                        $dato = $resultado4[0];
  
            $conect9 = new conexionMySql(); 
 
            $query9 = "UPDATE detalle SET disponiblequiebre='".$dato["DISPONIBLE"]."', fechahoradeteccionquiebre=CURRENT_TIMESTAMP WHERE id = ".$filaDetalle[0]." ";
          
            $resultado9 = $conect9->ejecuta($query9);
            


                         // $dato = $resultado4[0];
                         //  if ((($dato["DISPONIBLE"])<$cantidad) and (trim($filaDetalle[8])!="reemplazado")){
                         //    $error = "si";
                         //    $errorStock = "si";
                         //    $errorStockDetalle.= $filaDetalle[0]."///123".trim($filaDetalle[3])."///123".trim($filaDetalle[8])."///123".trim($cantidad)."///123".trim($dato["DISPONIBLE"])."///123".trim($dato["FAMILIA"])."_123_";
                         //  }




                     } 
                
        }

    // }

 
 
   $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
      $query = "INSERT INTO Wd_notavta (numord, codpro, canpro, prelis, codemp) VALUES (:nord, :cod, :cant, :pre, 3)";

      $compiled = oci_parse($conn, $query);

    oci_bind_by_name($compiled, ':nord', $numOrden);
    oci_bind_by_name($compiled, ':cod', $codigo);
    oci_bind_by_name($compiled, ':cant', $cantidad);
     
    oci_bind_by_name($compiled, ':pre', $precio);
    $resultado = oci_execute($compiled);

 
    if ($resultado==1){
    }else{
         $tipoError = "error insertando detalle id: ".$filaDetalle[0];
      $error = "si";
      // break;
    }
}

if (count($codigoDuplicado)==0){
    if ($error!="si"){
        if (strlen($oc["totalneto"])>0){
            if ($netoCalculado!=$oc["totalneto"]){
                $tipoError = "error neto no coincide";
                  $error = "si";
            }    
        }
    }
}

        $conectO3 = new conexionOracle(); 
        $query3 = "SELECT * FROM re_cctocli WHERE rutcli= ".$rut." AND cencos=".$this->_cencos."";
        $resultado3 = $conectO3->selectSimple($query3);

    if (count($resultado3[0])==0){
        $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
        $query3 = "INSERT INTO re_cctocli (rutcli, cencos, ccosto) VALUES (:rutcc, :cc_cc, :cco_cc)";
        $compiled = oci_parse($conn, $query3);

        oci_bind_by_name($compiled, ':rutcc', $rut);
        oci_bind_by_name($compiled, ':cc_cc', $this->_cencos);
        oci_bind_by_name($compiled, ':cco_cc', $this->_cencos);
 
        $resultado2 = oci_execute($compiled);
        if ($resultado2==1){

            
        }else{
             $tipoError = "error insertando cencos";
          $error = "si";
        }
    }




    if ($error!="si"){

        

  
           if (count($codigoDuplicado)>0){
              $conect = new conexionMySql(); 
           $query = "UPDATE pedido SET numord_tele_duplicado=''  WHERE id=".$oc[0]."";
           $resultado = $conect->ejecuta($query);
    
                for ($i=0; $i < ($mayorDuplicado-1); $i++) {


                        $codigoDuplicadoTemporal = array();
                        $codigoDuplicadoEncontradoTemporal1 = array();
                        if (count($filaDuplicadoEncontrado)>0){

                            for ($k=0; $k < count($filaDuplicadoEncontrado); $k++) {
                                $filaDupicado = $filaDuplicadoEncontrado[$k];
                                    if (!in_array(trim($filaDupicado[3]), $codigoDuplicadoEncontradoTemporal1)) {
                                 array_push($codigoDuplicadoEncontradoTemporal1, trim($filaDupicado[3]));
 
                                    }
 
                            }        


                        }
 
 

                        $filaDuplicadoEncontradoTemporal = array();
                        $codigoDuplicadoEncontradoTemporal = array();
                        $detalleTemporal = array();
                         for ($k=0; $k < count($filaDuplicadoEncontrado); $k++) { 
                               $filaDetalle = $filaDuplicadoEncontrado[$k];
                                $encontrado = "no";
                                $seGuarda = "si";
                                    for ($d=0; $d < count($codigoDuplicadoEncontradoTemporal1); $d++) { 
                                       $filaDupicado = $codigoDuplicadoEncontradoTemporal1[$d];
                                      
                                             if (trim($filaDetalle[3])==trim($filaDupicado)){
                                                 if (in_array(trim($filaDetalle[3]), $codigoDuplicadoEncontradoTemporal)) {
                                                    $seGuarda ="no";
                                                    array_push($filaDuplicadoEncontradoTemporal, $filaDetalle);
                                                } else{
                                                    array_push($codigoDuplicadoEncontradoTemporal, trim($filaDetalle[3]));
                                                     
                                                }
                                            }
             
                                    }
                                    if ($seGuarda == "si"){
                                        array_push($detalleTemporal, $filaDetalle);
                                    }
              
                             }




                          $filaDuplicadoEncontrado = $filaDuplicadoEncontradoTemporal;
                       // $dato= array($filaDuplicadoEncontrado);
                         $this->_detalleDuplicado = $detalleTemporal;
                     $this->_aceptarOrdenDuplicadaConBd();

                    
                }

                 $nuevoDescuento = $this->_corrigeDescuentoTelemarketingDuplicado($oc, $detalle, $numOrden, $dscto);

            $conect = new conexionMySql(); 
           $query = "UPDATE pedido SET numord_tele = ".$numOrden.", fechahoraaceptada=CURRENT_TIMESTAMP, idestado=4, cencos = '".$this->_cencos."', descuentocorregido = '".$nuevoDescuento."', flete = '".$totalFlete."', detalleflete = '".$detalleFlete."' WHERE id=".$oc[0]."";
           $resultado = $conect->ejecuta($query);
             return "bienDuplicado->".(count($codigoDuplicado)+1)."-".$nuevoDescuento;
             exit();
           }else{

            $nuevoDescuento = $this->_corrigeDescuentoTelemarketing($oc, $detalle, $numOrden, $dscto);
           $conect = new conexionMySql(); 
           $query = "UPDATE pedido SET numord_tele = ".$numOrden.", fechahoraaceptada=CURRENT_TIMESTAMP, idestado=4, cencos = '".$this->_cencos."', descuentocorregido = '".$nuevoDescuento."', flete = '".$totalFlete."', detalleflete = '".$detalleFlete."' WHERE id=".$oc[0]."";
           $resultado = $conect->ejecuta($query);
            return "bien->".$nuevoDescuento;
          exit();
           }

 
    }else{
    
        //borrar restos de oracle 
 $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
         $query = "DELETE FROM Wd_notavta WHERE numord = ".$numOrden."";
 
              $compiled = oci_parse($conn, $query);
 
        oci_execute($compiled);


 $conn2 = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
         $query2 = "DELETE FROM We_notavta WHERE numord = ".$numOrden."";
 
              $compiled2 = oci_parse($conn2, $query2);
 
        oci_execute($compiled2);

        if ($errorRelacion=="si"){
            return "error_relacion->".substr($errorRelacionDetalle, 0, -5);
            exit();
        }


        if ($errorStock=="si"){
            return "error_stock->".substr($errorStockDetalle, 0, -5);
            exit();
        }else{

               $conect2 = new conexionMySql(); 
               $query2 = "UPDATE pedido SET idestado=7, tipoerror='".$tipoError."', fechahoraerror=CURRENT_TIMESTAMP, cencos = '".$this->_cencos."' WHERE id=".$oc[0]."";
              $conect2->ejecuta($query2);
         return $tipoError;
        exit();  
        }



    }

}else{
    $tipoError = "error insertando cabecera";
  $conect2 = new conexionMySql(); 
  $query2 = "UPDATE pedido SET idestado=7, tipoerror='".$tipoError."', fechahoraerror=CURRENT_TIMESTAMP, cencos = '".$this->_cencos."' WHERE id=".$oc[0]."";
 $conect2->ejecuta($query2);
 return $tipoError;
    exit(); 
}

 
}




private function _corrigeDescuentoTelemarketingDuplicado($oc, $det, $num, $dscto) 
    { 
 $conect = new conexionMySql(); 
 $query = "SELECT numord_tele_duplicado FROM pedido WHERE id=".$oc[0]."";
 $resultado = $conect->selectSimple($query);

$array_num= array();
$dupli = explode("-", substr($resultado[0],1));

for ($i=0; $i < count($dupli); $i++) { 
    array_push($array_num, $dupli[$i]);
}

    array_push($array_num, $num);
 
 $brutoTlmkTotal = 0;
for ($i=0; $i < count($array_num); $i++) { 
    $num2 = $array_num[$i];


            require_once "../controlador/conexionOracle.php"; 
            $conectO3 = new conexionOracle(); 
            $query3 = "SELECT * FROM Wd_notavta WHERE NUMORD = ".$num2."";
            $resultado3 = $conectO3->selectSimple($query3);
            $totalTlmk = 0;
            
      
                for ($j=0; $j < count($resultado3); $j++) { 
                    $filaDetalleTlmk = $resultado3[$j];
                    $totalTlmk  = $totalTlmk + ($filaDetalleTlmk["CANPRO"]*$filaDetalleTlmk["PRELIS"]);
                }

          $dsctoTlmk = ($totalTlmk*($dscto/100));
          $brutoTlmk = round(round($totalTlmk-$dsctoTlmk) + (round($totalTlmk-$dsctoTlmk)*0.19));

$brutoTlmkTotal = $brutoTlmkTotal + $brutoTlmk;
  

}
  



            if ($brutoTlmkTotal==$oc["totalbruto"]){
                return "Sin_cambios";
            }


            if ($brutoTlmkTotal>$oc["totalbruto"]){
                $distancia = $brutoTlmkTotal-$oc["totalbruto"];
               for ($j=1; $j < 100; $j++) { 
                   $dsctoTemporal = $dscto + ($j/100); 
                   $dsctoTlmkTemporal = round($totalTlmk*($dsctoTemporal/100));
                   $brutoTlmkTemporal = ($totalTlmk-$dsctoTlmkTemporal) + round(($totalTlmk-$dsctoTlmkTemporal)*0.19);
                   $distanciaTemporal = $brutoTlmkTemporal-$oc["totalbruto"];
                   if ($brutoTlmkTemporal<=$oc["totalbruto"]){
                        $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
                        $query = "UPDATE We_notavta SET descli = ".$dsctoTemporal." WHERE NUMORD = ".$num."";
                        $compiled = oci_parse($conn, $query);
                        $resultado = oci_execute($compiled);
                        return $dsctoTemporal;
                   }
               }
            }else{
                return "Sin_cambios";
            }



 
}



private function _corrigeDescuentoTelemarketing($oc, $det, $num, $dscto) 
    { 
 
require_once "../controlador/conexionOracle.php"; 
$conectO3 = new conexionOracle(); 
$query3 = "SELECT * FROM Wd_notavta WHERE NUMORD = ".$num."";
$resultado3 = $conectO3->selectSimple($query3);

$totalTlmk = 0;
if (count($resultado3)==count($det)){
    for ($i=0; $i < count($resultado3); $i++) { 
        $filaDetalleTlmk = $resultado3[$i];
        $totalTlmk  = $totalTlmk + ($filaDetalleTlmk["CANPRO"]*$filaDetalleTlmk["PRELIS"]);
    }

$dsctoTlmk = ($totalTlmk*($dscto/100));
$brutoTlmk = round(round($totalTlmk-$dsctoTlmk) + (round($totalTlmk-$dsctoTlmk)*0.19));


if ($brutoTlmk==$oc["totalbruto"]){
    return "Sin_cambios";
}


if ($brutoTlmk>$oc["totalbruto"]){
    $distancia = $brutoTlmk-$oc["totalbruto"];
   for ($j=1; $j < 100; $j++) { 
       $dsctoTemporal = $dscto + ($j/100); 
       $dsctoTlmkTemporal = round($totalTlmk*($dsctoTemporal/100));
       $brutoTlmkTemporal = ($totalTlmk-$dsctoTlmkTemporal) + round(($totalTlmk-$dsctoTlmkTemporal)*0.19);
       $distanciaTemporal = $brutoTlmkTemporal-$oc["totalbruto"];
       if ($brutoTlmkTemporal<=$oc["totalbruto"]){
            $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
            $query = "UPDATE We_notavta SET descli = ".$dsctoTemporal." WHERE NUMORD = ".$num."";
            $compiled = oci_parse($conn, $query);
            $resultado = oci_execute($compiled);
            return $dsctoTemporal;
       }
   }
}else{
    return "Sin_cambios";
}


// if ($brutoTlmk<$oc["totalbruto"]){
//     $mejorDescuento = $dscto;
//     $distancia = $oc["totalbruto"]-$brutoTlmk;
//    for ($j=1; $j < 100; $j++) { 
//        $dsctoTemporal = $dscto - ($j/100); 
//        $dsctoTlmkTemporal = round($totalTlmk*($dsctoTemporal/100));
//        $brutoTlmkTemporal = ($totalTlmk-$dsctoTlmkTemporal) + round(($totalTlmk-$dsctoTlmkTemporal)*0.19);
//        $distanciaTemporal = $oc["totalbruto"] - $brutoTlmkTemporal;
//        if ($brutoTlmkTemporal<=$oc["totalbruto"]){
//         $mejorDescuento = $dsctoTemporal;
//        }else{
//             $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
//             $query = "UPDATE We_notavta SET descli = ".$mejorDescuento." WHERE NUMORD = ".$num."";
//             $compiled = oci_parse($conn, $query);
//             $resultado = oci_execute($compiled);
//             return $mejorDescuento;
//        }
//    }
// }


}else{
   return "error"; 
}




 
}



private function _aceptarOrdenDuplicadaConBd() 
    { 
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
  $error = "";
  $errorRelacion="";
  $tipoError ="";
 $array = $this->_detalleOCConBd();
$oc= $array[0];
 
$detalle=$this->_detalleDuplicado;
 
 
 

require_once "../controlador/conexionOracle.php"; 
 require_once "../controlador/conexionMySql.php"; 

  
     $conect = new conexionOracle();
    $query = "Select dm_ventas.notaweb_seq.nextval as Serie from dual";
    $resultado = $conect->selectSimple($query);
    $arr = $resultado[0];
    $numOrden = $arr["SERIE"];
    $dirFact = substr($oc[14], 0, 30); 
        if ((trim($oc["rutdescripcion"]) == "No hay") or (trim($oc["rutdescripcion"]) == "Encontrado, pero es un error conocido")) {
                    $rut = $oc["rut"];
                  }else{
                    $rut =  $oc["rutdescripcion"];
                  }

$dscto = abs($oc["descuento"])*100/$oc["totalneto"];
$dscto = round($dscto, 2);
 

$dscto = (float)$dscto;
 

         $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
                  $query = "INSERT INTO We_notavta (numord, fecord, facnom, rutcli, cencos, facdir, observ, tipweb, descli, ORDWEB) VALUES(:nord, trunc(sysdate), :ordweb, :rutcli, :cc, '0', :observ, '4', :dscto, :idbd)";
            

              $compiled = oci_parse($conn, $query);

if (strpos($_SERVER['REQUEST_URI'],'Desarrollo') !== false) {
    //Estoy en Desarrollo
      $rut = '22087581';
      $this->_cencos =0;
 }else{
    //Estoy en Productivo
}
 
 
if (strlen(trim($oc[4]))>500){
     $descri = substr(trim($oc[4]), 0, 475);
     $descri = $descri . " ...(truncado por robot)";
}else{
    $descri = trim($oc[4]);
}

 

oci_bind_by_name($compiled, ':idbd', $oc[0]);
oci_bind_by_name($compiled, ':nord', $numOrden);
oci_bind_by_name($compiled, ':rutcli', $rut);
oci_bind_by_name($compiled, ':cc', $this->_cencos);
 
oci_bind_by_name($compiled, ':ordweb', $oc[2]);
oci_bind_by_name($compiled, ':observ', $descri);
oci_bind_by_name($compiled, ':dscto', $dscto);
$resultado = oci_execute($compiled);

 
if ($resultado==1){
$netoCalculado = 0;
for ($i=0; $i < count($detalle); $i++) { 
  $filaDetalle = $detalle[$i];
 $netoCalculado = $netoCalculado + ($filaDetalle[5]*$filaDetalle[6]);
 
if (trim($filaDetalle[7])=="USD"){
  $filaDetalle[6] = $this->dolarApesos($filaDetalle[6], $oc[3]);
   $tipoError = "DUPLICADO: dolar";
      $error = "si";
      break;
}
 




   if (strlen(trim($filaDetalle[9]))>0){

        $precio = round($filaDetalle[6]/$filaDetalle[9]);
         $cantidad = round($filaDetalle[5]*$filaDetalle[9]);
         $codigo = strtoupper(trim($filaDetalle[3]));
   }else{

      $conect3 = new conexionOracle(); 
       $query3 = "SELECT unidad, iddimerc FROM gob_relacion where trim(codigo) = '".trim($filaDetalle[8])."'";
       $resultado3 = $conect3->selectSimple($query3);

       if (count($resultado3)>1){
        $tipoError = "DUPLICADO: duplicidad codigo chc: ".trim($filaDetalle[8]);
      $error = "si";
      $precio = 0;
      $cantidad = 0;
       break;
     }
    if (count($resultado3[0])==0){
        $tipoError = "DUPLICADO: error relacion codigo dimerc: ".trim($filaDetalle[3])." y codigo chc: ".trim($filaDetalle[8]);
      $error = "si";
      $errorRelacion = "si";
      $errorRelacionDetalle.= $filaDetalle[0]."///123".trim($filaDetalle[3])."///123".trim($filaDetalle[8])."_123_";
      $precio = 0;
      $cantidad = 0;
      $codigo = 0;
      }else{
        $dato = $resultado3[0];
         $precio = round($filaDetalle[6]/$dato["UNIDAD"]);
         $cantidad = round($filaDetalle[5]*$dato["UNIDAD"]);
         $codigo = $dato["IDDIMERC"];
     } 

 
   }


 $codigo = strtoupper(trim($codigo));

   $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
      $query = "INSERT INTO Wd_notavta (numord, codpro, canpro, prelis, codemp) VALUES (:nord, :cod, :cant, :pre, 3)";

      $compiled = oci_parse($conn, $query);

    oci_bind_by_name($compiled, ':nord', $numOrden);
    oci_bind_by_name($compiled, ':cod', $codigo);
    oci_bind_by_name($compiled, ':cant', $cantidad);
     
    oci_bind_by_name($compiled, ':pre', $precio);
    $resultado = oci_execute($compiled);

 
    if ($resultado==1){
    }else{
         $tipoError = "DUPLICADO: error insertando detalle id: ".$filaDetalle[0];
      $error = "si";
      break;
    }
}

 
        $conectO3 = new conexionOracle(); 
        $query3 = "SELECT * FROM re_cctocli WHERE rutcli= ".$rut." AND cencos=".$this->_cencos."";
        $resultado3 = $conectO3->selectSimple($query3);

    if (count($resultado3[0])==0){
        $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
        $query3 = "INSERT INTO re_cctocli (rutcli, cencos, ccosto) VALUES (:rutcc, :cc_cc, :cco_cc)";
        $compiled = oci_parse($conn, $query3);

        oci_bind_by_name($compiled, ':rutcc', $rut);
        oci_bind_by_name($compiled, ':cc_cc', $this->_cencos);
        oci_bind_by_name($compiled, ':cco_cc', $this->_cencos);
 
        $resultado2 = oci_execute($compiled);
        if ($resultado2==1){

            
        }else{
             $tipoError = "DUPLICADO: error insertando cencos";
          $error = "si";
        }
    }

 


    if ($error!="si"){
           $conect = new conexionMySql(); 
           $query = "UPDATE pedido SET numord_tele_duplicado = CONCAT(numord_tele_duplicado,  '-', ".$numOrden.")  WHERE id=".$oc[0]."";
           $resultado = $conect->ejecuta($query);
           
          return "bien";
          exit();
    }else{
    
        //borrar restos de oracle 
 $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
         $query = "DELETE FROM Wd_notavta WHERE numord = ".$numOrden."";
 
              $compiled = oci_parse($conn, $query);
 
        oci_execute($compiled);


 $conn2 = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
         $query2 = "DELETE FROM We_notavta WHERE numord = ".$numOrden."";
 
              $compiled2 = oci_parse($conn2, $query2);
 
        oci_execute($compiled2);

        if ($errorRelacion=="si"){
            return "error_relacion->".substr($errorRelacionDetalle, 0, -5);
            exit();
        }else{

               $conect2 = new conexionMySql(); 
               $query2 = "UPDATE pedido SET idestado=7, tipoerror='".$tipoError."', fechahoraerror=CURRENT_TIMESTAMP, cencos = '".$this->_cencos."' WHERE id=".$oc[0]."";
              $conect2->ejecuta($query2);
         return $tipoError;
        exit();  
        }



    }

}else{
    $tipoError = "DUPLICADO: error insertando cabecera";
  $conect2 = new conexionMySql(); 
  $query2 = "UPDATE pedido SET idestado=7, tipoerror='".$tipoError."', fechahoraerror=CURRENT_TIMESTAMP, cencos = '".$this->_cencos."' WHERE id=".$oc[0]."";
 $conect2->ejecuta($query2);
 return $tipoError;
    exit(); 
}

 
}



private function _vincularConBd() 
    { 

if ($this->_tipo ==1){
return "proceso de vinculacion1!".$this->_rut;
}else{
 return "proceso de vinculacion2!".$this->_rut; 
}



 
}


private function _deshacerOrdenConBd() 
    { 
 

require_once "../controlador/conexionOracle.php"; 
 $conectO = new conexionOracle(); 
    $query = "select * from WE_NOTAVTA where NUMORD = ".trim($this->_numTelemarketing)."";
    $asdf = $conectO->selectSimple($query);

    $dato = $asdf[0];

if ($dato['NUMORD'] > 1){
  if ($dato['NUMNVT'] > 1){

     return "ERROR, El pedido ya tiene una nota de venta numero: ".$dato['NUMNVT'].", no es posible revertir el envio!";


  }else{

     $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
         $query = "DELETE FROM Wd_notavta WHERE numord = ".trim($this->_numTelemarketing)."";
 
              $compiled = oci_parse($conn, $query);
 
        oci_execute($compiled);


 $conn2 = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
         $query2 = "DELETE FROM We_notavta WHERE numord = ".trim($this->_numTelemarketing)."";
 
              $compiled2 = oci_parse($conn2, $query2);
 
        oci_execute($compiled2);

 require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
$insert = "UPDATE pedido SET idestado=2, numord_tele=NULL, fechahoraaceptada=NULL WHERE id= ".$this->_idPedido."";
      $resultado = $conect->ejecuta($insert);
  
return "El pedido no tiene nota de venta, por lo que fue borrado de oracle y devuelto a la bandeja de pendientes de su ejecutivo!";


  }

}else{
  return "ERROR, El pedido no se encuentra en oracle!";
}






}





private function _rechazarConBd() 
    { 

        if (strpos($this->_cencosTexto,"=>") !== false) {
            $cc = explode("=>", $this->_cencosTexto);
        }else{
            $cc[1] = $this->_cencosTexto;
        }

      
 require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
$insert = "UPDATE pedido SET cencoselegido = ".$this->_cencos.", cencos = '".trim($cc[1])."', idmotivo = ".$this->_idMotivo.", idestado=3, comentarioejecutivo='".$this->_comentarioEjecutivo."', fechahorarechazada=CURRENT_TIMESTAMP WHERE id= ".$this->_idPedido."";
      $resultado = $conect->ejecuta($insert);
  
 return "go";
}

 

private function _guardaFilaCodigoConBd() 
    { 

$conn2 = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
    $query2 = "select DESPRO, GETLINEA(CODPRO) LINEA, GETFAMILIA(CODPRO) FAMILIA, GETMARCA(CODPRO) MARCA from MA_PRODUCT where CODPRO = '".trim($this->_idDimerc)."'";
   $compiled2 = oci_parse($conn2, $query2);
oci_execute($compiled2); 

 $row = oci_fetch_array($compiled2, OCI_ASSOC+OCI_RETURN_NULLS);
 
 
 if (count($row)==4){
 $despro = $row['DESPRO'];
$linea = $row['LINEA'];
$familia = $row['FAMILIA'];
$marca = $row['MARCA'];
}else{
$despro = "";
$linea = "";
$familia = "";
$marca = "";
}


 $conn = oci_connect("dm_ventas", "dimerc", "10.10.20.1/PROD"); 
        $query3 = "INSERT INTO gob_relacion(iddimerc, codigo, unidad, idtipo, descripcion, linea, familia, marca) VALUES (:dim, :cod, :uni, :tip, :des, :li, :fam, :mar)";
        $compiled = oci_parse($conn, $query3);

        oci_bind_by_name($compiled, ':dim', $this->_idDimerc);
        oci_bind_by_name($compiled, ':cod', $this->_idChc);
        oci_bind_by_name($compiled, ':uni', $this->_unidad);
        oci_bind_by_name($compiled, ':tip', $this->_tipoCodigo);
        oci_bind_by_name($compiled, ':des', $despro);
        oci_bind_by_name($compiled, ':li', $linea);
        oci_bind_by_name($compiled, ':fam', $familia);
        oci_bind_by_name($compiled, ':mar', $marca);

        $resultado2 = oci_execute($compiled);

 
  }




// private function _insertarPedidoConBd() 
//     { 
//     require_once "clases/controlador/conexionMySql.php"; 
//     $conect = new conexion(); 

//     $query = "SELECT id FROM pedido WHERE buyerordernumber='".$this->_ordenDeCompra."'";

//     $resultado = $conect->selectSimple($query);

//     if ($resultado == 'LaConsultaNoTieneRegistros'){
//       $rutfinal =  str_replace("-", "", trim($this->_rut));
//       $rutfinal =  str_replace(".", "", $rutfinal);
//       $rutsindv = substr ($rutfinal, 0, strlen($rutfinal) - 1);
//       $dv = $rutfinal[strlen($rutfinal)-1];
//       $rutDescr = $this->buscarRutEnDescripcion($this->_descripcion, $rutfinal);

//        if (($rutDescr!="No hay!") and ($rutDescr!="Encontrado, pero es un error conocido!")){
//         $ejecutivo = $this->dameEjecutivo( substr ($rutDescr, 0, strlen($rutDescr) - 1));
//         $rs = $this->dameRs( substr ($rutDescr, 0, strlen($rutDescr) - 1));
//         $cc = $this->dameCC( substr ($rutDescr, 0, strlen($rutDescr) - 1));
//         $rutEjecutivo=$ejecutivo["RUTVENDEDOR"];
//         $usuarioEjecutivo=$ejecutivo["USUARIO"];
//       }else{
//         $ejecutivo = $this->dameEjecutivo($rutsindv);
//         $rs = $this->dameRs($rutsindv);
//         $cc = $this->dameCC($rutsindv);
//         $rutEjecutivo=$ejecutivo["RUTVENDEDOR"];
//         $usuarioEjecutivo=$ejecutivo["USUARIO"];
//       }
// // 12934
  

// // select * from pedido where buyerordernumber 5817-16-CM12
//       if (strlen(trim($rutEjecutivo))<6){
//         $estado = 1;
//       }else{
//         $estado = 2;
//       }

//       $insert = "INSERT INTO pedido(idestado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, nombre, correo, telefono, rutejecutivo, usuarioejecutivo, rutdescripcion, idmotivo, razonsocial, totalneto, totalbruto, descuento, direccionfactura) VALUES (".$estado.", '".$this->_ordenDeCompra."', '".$this->_fecha."', '".$this->_descripcion."', ".$rutsindv.", '".$dv."', '".$this->arregla($this->_direNom)."', '".$this->arregla($this->_direCalle)."', '".$this->_direCiudad."', '".$this->arregla($this->_direRegion)."', '".$this->_nombre."', '".$this->_correo."', '".$this->_telefono."', '".$rutEjecutivo."', '".$usuarioEjecutivo."', '".substr ($rutDescr, 0, strlen($rutDescr) - 1)."', 1, '".$rs."', '".$cc."', '".$this->_neto."', '".$this->_bruto."', '".$this->_descuentototal."', '".$this->_direNomF.": ".$this->_direCalleF.", ".$this->_direCalleF.", ".$this->_direRegionF."') returning id";
//       $resultado = $conect->ejecuta($insert);
 
//       if ($resultado>0){

//         for ($i=0; $i < count($this->_codigo); $i++) { 
//           $asdf = explode(")", $this->_detalle[$i]);
//           $codigochc = substr ($asdf[0], 1, strlen($asdf[0]));
//           $descr = trim($asdf[1]);
//           $cod = explode(":", $this->_codigo[$i]);
//           $conect = new conexion();
//           $insert2 = "INSERT INTO detalle(idpedido, idtexto, idcodigo, descripcion, cantidad, precio, moneda, descuento, codigochc) VALUES (".$resultado[0].", '".$this->_texto[$i]."', '".trim($cod[1])."', '".$descr."', '".$this->_cantidad[$i]."', '".$this->_precio[$i]."', '".$this->_moneda[$i]."', '".$this->_descuento[$i]."', '".$codigochc."')";
//             $conect->ejecuta($insert2);
//         }
//         return "PEDIDO GUARDADO EN BASE DE DATOS!";
//       }else{
//         return "Un error ha ocurrido, contacte al administrador!".$insert;        
//       }
 
//     }else{
//       return "YA FUE PROCESADO!";
//     }
// }


private function _listadoPedidoConBd() 
    { 

session_name("robotChilecompra");
@session_start();  
    require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
$arrayPedido = array();

$str = "rutejecutivo='".$_SESSION['rut_usu']."' ";
$reemplazo = $_SESSION['reemplazo'];
if ($reemplazo!="LaConsultaNoTieneRegistros"){
  
  
  for ($i=0; $i < count($reemplazo); $i++) { 
    $fila = $reemplazo[$i];
    $str .= " OR rutejecutivo='".$fila["rutreemplazado"]."' ";
  }
}


switch($this->_categoria){
case 'SinAsignar':
  if ($this->_tipoListado =="Mios") {
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial from pedido, estado WHERE estado.id = pedido.idestado AND rutejecutivo='".$_SESSION['rut_usu']."' AND idestado=1";
  }else{
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, totalbruto from pedido, estado WHERE estado.id = pedido.idestado AND idestado=1";
  }
 break;
case 'Pendientes':
  if ($this->_tipoListado =="Mios") {
     // $this->_limpiarYaHechosConBd();
    if (strlen(trim($_SESSION['rut_usu']))<5){
        return "caducado";
    }
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, fechareferencia from pedido, estado WHERE estado.id = pedido.idestado AND (".$str.") AND idestado=2 ORDER BY orderissuedate";
  }else{
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, fechareferencia from pedido, estado WHERE estado.id = pedido.idestado AND idestado=2";
  }
 
 break;
case 'Rechazadas':
  if ($this->_tipoListado =="Mios") {
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, cencoselegido, cencos from pedido, estado WHERE estado.id = pedido.idestado AND rutejecutivo='".$_SESSION['rut_usu']."' AND idestado=3";
  }else{
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, motivo.nombre, comentarioejecutivo, totalbruto, cencoselegido, cencos from pedido, estado, motivo WHERE motivo.id=pedido.idmotivo and estado.id = pedido.idestado AND idestado=3";
  }
 break;
case 'Procesadas':
  if ($this->_tipoListado =="Mios") {
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, cencos from pedido, estado WHERE estado.id = pedido.idestado AND rutejecutivo='".$_SESSION['rut_usu']."' AND idestado=4";
  }else{
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, cencos, numord_tele from pedido, estado WHERE estado.id = pedido.idestado AND idestado=4";
  }
 break;
 case 'Errores':
  if ($this->_tipoListado =="Mios") {
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, cencos from pedido, estado WHERE estado.id = pedido.idestado AND rutejecutivo='".$_SESSION['rut_usu']."' AND idestado=7";
  }else{
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, cencos from pedido, estado WHERE estado.id = pedido.idestado AND idestado=7";
  }
 break;
case 'Asignadas':
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, totalbruto from pedido, estado WHERE estado.id = pedido.idestado AND idestado=2";
 break;
//  case 'reasignar':
//     $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, fechareferencia from pedido, estado WHERE estado.id = pedido.idestado AND fechahorareasignacion>'2016-01-18' AND (idestado=2 or idestado=1)";
 
//  $resultado = $conect->selectMultiple($query);


//  for ($i=0; $i < count($resultado); $i++) { 
//     $fila = $resultado[$i];

//                   if ((trim($fila[15]) == "No hay") or (trim($fila[15]) == "Encontrado, pero es un error conocido")or (trim($fila[15]) == "Descartado")){
//                     $rut = $fila[5];
//                   }else{
//                     $rut = $fila[15];
//                   }

// $ejecutivo = $this->dameEjecutivoDesdePortal(trim($rut));
//         $ejecutivo = $ejecutivo[0];
//         $rutEjecutivo=$ejecutivo["RUTVENDEDOR"];
//         $usuarioEjecutivo=$ejecutivo["USUARIO"];
//         $rs =  $this->dameRsDesdePortal(trim($rut));

//         if (strlen(trim($rutEjecutivo))<6){
//         }else{
//           $query2 = "UPDATE pedido SET idestado=2, rutejecutivo='".$rutEjecutivo."', usuarioejecutivo='".$usuarioEjecutivo."', razonsocial='".$rs."' WHERE id = ".$fila[0]." ";
//         }
//         $conect2 = new conexionMySql();
//         $resultado2 = $conect->ejecuta($query2);


//  }

 


//  break;

   case 'duplicado':
    $query = "SELECT COUNT(buyerordernumber), buyerordernumber FROM pedido GROUP BY buyerordernumber HAVING COUNT(buyerordernumber) > 1 ORDER BY COUNT(buyerordernumber)";
 
 $resultado = $conect->selectMultiple($query);

if ($resultado!="LaConsultaNoTieneRegistros"){



 for ($i=0; $i < count($resultado); $i++) { 
    $fila = $resultado[$i];


    $query2 = "SELECT * FROM pedido WHERE trim(buyerordernumber)='".trim($fila[1])."' order by idestado desc";
       
        $conect2 = new conexionMySql();
        $resultado2 = $conect2->selectMultiple($query2);



             for ($j=1; $j < count($resultado2); $j++) { 
                $fila2 = $resultado2[$j];


                $query3 = "DELETE FROM pedido WHERE id=".trim($fila2[0])."";
                   
                    $conect3 = new conexionMySql();
                    $resultado3 = $conect3->ejecuta($query3);


 
             }




  
 }






}


 


 break;

 case 'Errores_Admin':
    $query = "SELECT pedido.id, estado.nombre as estado, buyerordernumber, orderissuedate, referencedescription, rut, dv, direccion, calle, ciudad, region, pedido.nombre, correo, telefono, fechahoracreacion, rutdescripcion, rutejecutivo, usuarioejecutivo, razonsocial, totalbruto, tipoerror from pedido, estado WHERE estado.id = pedido.idestado AND idestado=7";
 break;
 case 'Telemarketing':
    require_once "../controlador/conexionOracle.php"; 
    $conect2 = new conexionOracle(); 
    $query2 = "SELECT numord, fecord, facnom, rutcli, cencos, facdir, observ, tipweb, descli, ORDWEB FROM We_notavta WHERE numord= 1851944";
    $resultado2 = $conect2->selectSimple($query2);
    return $resultado2;
 break;
}
 
    $resultado = $conect->selectMultiple($query);
 if (strpos($_SERVER['REQUEST_URI'],'Desarrollo') !== false) {
    //Estoy en Desarrollo
  }else{
    //Estoy en Productivo
     // $this->_revisarRobotConBd();
}
 return $resultado;
 
}


private function _limpiarYaHechosConBd() {
    session_name("robotChilecompra");
    @session_start(); 
    require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
    $arrayPedido = array();
    $query = "SELECT id, buyerordernumber from pedido WHERE rutejecutivo='".$_SESSION['rut_usu']."' AND idestado=2 ORDER BY orderissuedate";
    $resultado = $conect->selectMultiple($query);

    require_once "../controlador/conexionOracle.php"; 
    for ($i=0; $i < count($resultado); $i++) { 
        $listo="no";
        $fila = $resultado[$i];
          $conectO = new conexionOracle(); 
            $query2 = "SELECT * FROM EN_NOTAVTA WHERE trim(docaso)= '".trim($fila[1])."'";
            $resultado2 = $conectO->selectSimple($query2);
         
        if (count($resultado2[0])!=0){
           
        $conect3 = new conexionMySql(); 
        $query3 = "UPDATE pedido SET idestado=8 WHERE id=".$fila[0]."";
         $conect3->ejecuta($query3);
          $listo="si";
        }

        if ($listo=="no"){
                $conectO3 = new conexionOracle(); 
            $query3 = "SELECT * FROM We_notavta WHERE trim(FACNOM)= '".trim($fila[1])."'";
            $resultado3 = $conectO3->selectSimple($query3);

                 
                if (count($resultado3[0])!=0){
                   
                $conect3 = new conexionMySql(); 
                $query3 = "UPDATE pedido SET idestado=8 WHERE id=".$fila[0]."";
                 $conect3->ejecuta($query3);
                }

        }




        if ($listo=="no"){


        $addr = "www.mercadopublico.cl/wsoc/wsGetOc.asmx?WSDL";
        $usr = "966708409";
        $pass = "chilecompra523";
        $meth = "GenerateXMLOCbyCode";
         require_once('../../webServices/nusoap/nusoap.php');
                $client = new nusoap_client("http://$addr", true);

                $error = $client->getError();
                if ($error) {
                    return "Constructor error".$error;
                }
                $client->setCredentials($usr, $pass, "basic");
                $result = "";

                if ($client) {
                    $result = $client->call($meth, array("porCode" => trim($fila[1])));
                }
                if ($client->fault) {
                    return "Fault";
                    print_r($result);
                }
                else {
                    $error = $client->getError();
                    if ($error){
                        echo "Error after call: ".$error."--->".$fila[1];
                    }
                    else{
                        if (strpos($result["GenerateXMLOCbyCodeResult"],'Se ha producido un error') !== false) {
                            return 'no_existe';
                        }else{
                            $xml = simplexml_load_string(utf8_encode($result["GenerateXMLOCbyCodeResult"]));
                            $listadoCotizaciones = $xml->OrdersList->Order;
                            $this->_xmlCotizacion = $listadoCotizaciones[0];

                                 $estadoCotizacion = $this->_xmlCotizacion->OrderSummary->SummaryNote; 
                                  if ((trim($estadoCotizacion)=="OC Cancelada") ){
                                       $conect = new conexionMySql(); 
                                       $query2 = "UPDATE pedido SET idestado = 9 WHERE id=".$fila[0]."";
                                       $conect->ejecuta($query2);

                                  }


                        
                        }
                       
                         
                    }
                }


                  
        }


    }

 
   
}

 
private function _limpiarYaHechosMasivoConBd() {
    session_name("robotChilecompra");
    @session_start(); 
    require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql(); 
    $arrayPedido = array();
    $query = "SELECT id, buyerordernumber from pedido WHERE idestado=2 ORDER BY orderissuedate";
    $resultado = $conect->selectMultiple($query);

    require_once "../controlador/conexionOracle.php"; 
    for ($i=0; $i < count($resultado); $i++) { 
        $fila = $resultado[$i];
          $conectO = new conexionOracle(); 
            $query2 = "SELECT * FROM EN_NOTAVTA WHERE trim(docaso)= '".trim($fila[1])."'";
            $resultado2 = $conectO->selectSimple($query2);
         
        if (count($resultado2[0])!=0){
           
        $conect3 = new conexionMySql(); 
        $query3 = "UPDATE pedido SET idestado=8 WHERE id=".$fila[0]."";
         $conect3->ejecuta($query3);
        }


        $conectO3 = new conexionOracle(); 
    $query3 = "SELECT * FROM We_notavta WHERE trim(FACNOM)= '".trim($fila[1])."'";
    $resultado3 = $conectO3->selectSimple($query3);

         
        if (count($resultado3[0])!=0){
           
        $conect3 = new conexionMySql(); 
        $query3 = "UPDATE pedido SET idestado=8 WHERE id=".$fila[0]."";
         $conect3->ejecuta($query3);
        }



    }
 
}


private function _listadoMotivosConBd() {

    require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql();
    $query = "select * from motivo where id>1 order by id";
    $asdf = $conect->selectMultiple($query);

    return $asdf;

   
}

 // select * from pedido where date(orderissuedate) = '2014-11-29'
 

public function dameEjecutivoDesdePortal($rutCli) {

      require_once "../controlador/conexionOracle.php"; 
    $conectO = new conexionOracle(); 
    $query = "select codven RutVendedor, getuseridx(codven) Usuario from en_cliente where rutcli = ".$rutCli." and codemp = 3";
    $asdf = $conectO->selectSimple($query);

    return $asdf;
 
}

public function dolarApesos($valor, $fecha) {

$fe = explode(" ", $fecha);

 require_once "../controlador/conexionMySql.php"; 



for ($i=0; $i < 10; $i++) { 
  $fecha =  date('Y-m-d', strtotime($fe[0]. ' - '.$i.' days'));
  $conect = new conexionMySql();
  $query = "select valor from dolar where fecha = '".$fecha."'";
  $asdf = $conect->selectSimple($query);
 
  if (is_numeric($asdf[0])){
    //metodo de redondeo?
    return round($asdf[0] * $valor);
  } 
}


    
 
}

public function dameEjecutivo($rutCli) {
    if ($rutCli=="61935400") {
        $temporal = array();
        $respuesta = array();
        $temporal["RUTVENDEDOR"]="11636702";
        $temporal["USUARIO"]="CCEBALLOS";
        $respuesta[0]=$temporal;

       return $respuesta;
    }
    if ($rutCli=="69255300") {
        $temporal = array();
        $respuesta = array();
        $temporal["RUTVENDEDOR"]="11492409";
        $temporal["USUARIO"]="DNUNEZ";
        $respuesta[0]=$temporal;

       return $temporal;
    }
      require_once "../clases/controlador/conexionOracle.php"; 
    $conectO = new conexionOracle(); 
    $query = "select codven RutVendedor, getuseridx(codven) Usuario from en_cliente where rutcli = ".$rutCli." and codemp = 3";
    $asdf = $conectO->selectSimple($query);

    return $asdf;
 
}

public function dameRs($rutCli) {

      require_once "../clases/controlador/conexionOracle.php"; 
    $conectO = new conexionOracle(); 
    $query = "SELECT RAZONS FROM EN_CLIENTE WHERE rutcli = ".$rutCli."";
    $asdf = $conectO->selectSimple($query);
    if (count($asdf)>0){
        $aa=$asdf[0];
        return $aa["RAZONS"];
    }else{
        return "";
    }
 
}

public function dameRsDesdePortal($rutCli) {

      require_once "../controlador/conexionOracle.php"; 
    $conectO = new conexionOracle(); 
    $query = "SELECT RAZONS FROM EN_CLIENTE WHERE rutcli = ".$rutCli."";
    $asdf = $conectO->selectSimple($query);
    if (count($asdf)>0){
        $aa=$asdf[0];
        return $aa["RAZONS"];
    }else{
        return "";
    }
 
}

public function dameCCdesdePortal($rutCli) {
    $detalle='';
      require_once "../controlador/conexionOracle.php"; 
    $conectO = new conexionOracle(); 
    $query = "SELECT CENCOS FROM EN_CLIENTE WHERE rutcli = ".$rutCli."";
    $asdf = $conectO->selectSimple($query);
    $aa=$asdf[0];

    $conectO2 = new conexionOracle(); 
    $query2 = "SELECT CENCOS, RUTCLI, DESCCO FROM DE_CLIENTE WHERE rutcli = ".$rutCli." and codemp = 3 order by CENCOS";
    $asdf2 = $conectO2->selectSimple($query2);
    for ($i=0; $i < count($asdf2); $i++) { 
     $ss=$asdf2[$i];
     $detalle.=$ss["CENCOS"]."*123".$ss["DESCCO"]."$123";
    }
    $detalle = substr ($detalle, 0, strlen($detalle) - 4);
    return $detalle;
 
}



public function _traerTodoDeEstaFamiliaConBd() {
           require_once "../controlador/conexionOracle.php"; 
    $conectO = new conexionOracle();

 
        $query = "select a.codpro, a.despro, b.stocks- b.stkcom STOCK, getfamilia(a.codpro) FAMILIA 
                        from ma_product a, re_bodprod_dimerc b
                        where a.codpro = b.codpro
                        and b.codemp= 3 
                        and b.codbod = 1
                        and a.ESTPRO = 1
                        and getfamilia(a.codpro) ='".$this->_familia."'";


 
 
    $asdf = $conectO->selectSimple($query);

    $array_respuesta = array();

    for ($i=0; $i < count($asdf); $i++) { 
        $fila = $asdf[$i];
        $conn = oci_connect("Dm_transfeR_web", "dm_ventas", "10.10.20.1/PROD"); 
        $sql = "select GET_PRECIOPROD(".$this->_rut.", ".$this->_cencos.", a.codpro, 3) PRECIO from ma_product a WHERE codpro = '".$fila['CODPRO']."' ";  
                $stid = oci_parse($conn, $sql);  
                oci_execute($stid);
                $resultado2 = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);

                if (($resultado2['PRECIO']>0) and ($resultado2['PRECIO']<=$this->_precio)) {
                    $fila['PRECIO'] = number_format($resultado2['PRECIO'], 0, ",", ".");
                    array_push($array_respuesta, $fila);
                }


    }
    
    $json = json_encode($array_respuesta);
    return $json;
     
 
}


public function _dameSugrenciaProdutoConBd() {
           require_once "../controlador/conexionOracle.php"; 
    $conectO = new conexionOracle();

 
        $query = "select a.codpro, a.despro, b.stocks- b.stkcom STOCK, getfamilia(a.codpro) FAMILIA 
                        from ma_product a, re_bodprod_dimerc b
                        where a.codpro = b.codpro
                        and b.codemp= 3 
                        and b.codbod = 1
                        and a.ESTPRO = 1
                        and getfamilia(a.codpro) ='".$this->_familia."'";


 
 
    $asdf = $conectO->selectSimple($query);

    $array_respuesta = array();

    for ($i=0; $i < count($asdf); $i++) { 
        $fila = $asdf[$i];
        $conn = oci_connect("Dm_transfeR_web", "dm_ventas", "10.10.20.1/PROD"); 
        $sql = "select GET_PRECIOPROD(".$this->_rut.", ".$this->_cencos.", a.codpro, 3) PRECIO from ma_product a WHERE codpro = '".$fila['CODPRO']."' ";  
                $stid = oci_parse($conn, $sql);  
                oci_execute($stid);
                $resultado2 = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);

                if (($resultado2['PRECIO']>0) ) {
                    $fila['PRECIO'] = number_format($resultado2['PRECIO'], 0, ",", ".");
                    array_push($array_respuesta, $fila);
                }


    }
    



    return $array_respuesta;
 
}



public function _traerListadoFamiliaConBd() {


   require_once "../controlador/conexionOracle.php"; 
    $conectO = new conexionOracle();

 
        $query = "select DISTINCT getfamilia(a.codpro) FAMILIA 
                        from ma_product a, re_bodprod_dimerc b
                        where a.codpro = b.codpro
                        and b.codemp= 3 
                        and b.codbod = 1
                        and a.ESTPRO = 1";


 
 
    $asdf = $conectO->selectSimple($query);
    $json = json_encode($asdf);
    return $json;
  
}



public function dameCC($rutCli) {
    $detalle='';
      require_once "clases/controlador/conexionOracle.php"; 
    $conectO = new conexionOracle(); 
    $query = "SELECT CENCOS FROM EN_CLIENTE WHERE rutcli = ".$rutCli."";
    $asdf = $conectO->selectSimple($query);
    $aa=$asdf[0];

    $conectO2 = new conexionOracle(); 
    $query2 = "SELECT CENCOS, RUTCLI, DESCCO FROM DE_CLIENTE WHERE rutcli = ".$rutCli." and codemp = 3 order by CENCOS";
    $asdf2 = $conectO2->selectSimple($query2);
    for ($i=0; $i < count($asdf2); $i++) { 
     $ss=$asdf2[$i];
     $detalle.=$ss["CENCOS"]."*123".$ss["DESCCO"]."$123";
    }
    $detalle = substr ($detalle, 0, strlen($detalle) - 4);
    return $detalle;
 
}

private function listadoErroresConocidos() {
    require_once "../clases/controlador/conexionMySql.php"; 
    $conect = new conexionMySql();
    $query = "select * from error";
    $asdf = $conect->selectMultiple($query);

    return $asdf;

}

private function listadoErroresConocidosDesdePortal() {
    require_once "../controlador/conexionMySql.php"; 
    $conect = new conexionMySql();
    $query = "select * from error";
    $asdf = $conect->selectMultiple($query);

    return $asdf;

}


public function buscarRutEnDescripcion($ref, $rut){

$matrizError = $this->listadoErroresConocidos();

// $ref = eregi_replace("[\n|\r|\n\r]", ' ', $ref); 
$ref = preg_replace('<br>', '', $ref);
$ref = preg_replace('<BR>', '', $ref);
$ref = str_replace(",", " ", $ref);
$ref = preg_replace('!\s+!', ' ', $ref);
$palabras = explode(" ",trim($ref));
$numeros = array();
for ($i=0; $i < count($palabras); $i++){ 
  $fila = trim($palabras[$i]);



  $trozoX = filter_var($fila, FILTER_SANITIZE_NUMBER_INT);
  $trozoX2 =  str_replace("-", "", $trozoX);
  $trozoX3 =  str_replace(".", "", $trozoX2);
  $trozoX4 =  substr ($trozoX3, 0, strlen($trozoX3) - 1);
 

  $encontrado = "no";
  if ($matrizError!="LaConsultaNoTieneRegistros"){

  for ($k=0; $k < count($matrizError); $k++) { 
    $filaError = $matrizError[$k];

    if (trim($trozoX4) == trim($filaError[1])){
      $encontrado = "si";
      
     }
  }


  }


if ($encontrado == "no"){

    if (strlen(trim($fila))>6){
          if ((trim($fila[strlen($fila)-1])=="K") or (trim($fila[strlen($fila)-1]=="k"))){
           
            $trozo = substr ($fila, 0, strlen($fila) - 1);

            $soloNumero = filter_var($trozo, FILTER_SANITIZE_NUMBER_INT);
            if (strlen(trim($soloNumero))>0){

              array_push($numeros, trim($fila));
            }

          }else{
            $soloNumero = filter_var($palabras[$i], FILTER_SANITIZE_NUMBER_INT);
            if (strlen(trim($soloNumero))>0){
              array_push($numeros, trim($soloNumero));
            } 
          }
    }



}else{
  return "Encontrado, pero es un error conocido!";
}


 
}
 

for ($j=0; $j < count($numeros); $j++) { 
  $rutfinal =  str_replace("-", "", $numeros[$j]);
  $rutfinal =  str_replace(".", "", $rutfinal);
  if ((strlen(trim($rutfinal))==8) or(strlen(trim($rutfinal))==9)){
    if ($this->validaRut($rutfinal)=="true"){
      $rutfinal2 =  str_replace("-", "", trim($rut));
      $rutfinal2 =  str_replace(".", "", $rutfinal2);
      if($rutfinal==$rutfinal2){
        return  $rutfinal;
        break;
      }else{
        return $rutfinal;
        break;
      }
      
    }
  }
  
}

 return "No hay!";

}


public function buscarRutEnDescripcionDesdePortal($ref, $rut){

$matrizError = $this->listadoErroresConocidosDesdePortal();

// $ref = eregi_replace("[\n|\r|\n\r]", ' ', $ref); 
$ref = preg_replace('<br>', '', $ref);
$ref = preg_replace('<BR>', '', $ref);
$ref = str_replace(",", " ", $ref);
$ref = preg_replace('!\s+!', ' ', $ref);
$palabras = explode(" ",trim($ref));
$numeros = array();
for ($i=0; $i < count($palabras); $i++){ 
  $fila = trim($palabras[$i]);



  $trozoX = filter_var($fila, FILTER_SANITIZE_NUMBER_INT);
  $trozoX2 =  str_replace("-", "", $trozoX);
  $trozoX3 =  str_replace(".", "", $trozoX2);
  $trozoX4 =  substr ($trozoX3, 0, strlen($trozoX3) - 1);
 

  $encontrado = "no";
  if ($matrizError!="LaConsultaNoTieneRegistros"){

  for ($k=0; $k < count($matrizError); $k++) { 
    $filaError = $matrizError[$k];

    if (trim($trozoX4) == trim($filaError[1])){
      $encontrado = "si";
      
     }
  }


  }


if ($encontrado == "no"){

    if (strlen(trim($fila))>6){
          if ((trim($fila[strlen($fila)-1])=="K") or (trim($fila[strlen($fila)-1]=="k"))){
           
            $trozo = substr ($fila, 0, strlen($fila) - 1);

            $soloNumero = filter_var($trozo, FILTER_SANITIZE_NUMBER_INT);
            if (strlen(trim($soloNumero))>0){

              array_push($numeros, trim($fila));
            }

          }else{
            $soloNumero = filter_var($palabras[$i], FILTER_SANITIZE_NUMBER_INT);
            if (strlen(trim($soloNumero))>0){
              array_push($numeros, trim($soloNumero));
            } 
          }
    }



}else{
  return "Encontrado, pero es un error conocido!";
}


 
}
 

for ($j=0; $j < count($numeros); $j++) { 
  $rutfinal =  str_replace("-", "", $numeros[$j]);
  $rutfinal =  str_replace(".", "", $rutfinal);
  if ((strlen(trim($rutfinal))==8) or(strlen(trim($rutfinal))==9)){
    if ($this->validaRut($rutfinal)=="true"){
      $rutfinal2 =  str_replace("-", "", trim($rut));
      $rutfinal2 =  str_replace(".", "", $rutfinal2);
      if($rutfinal==$rutfinal2){
        return  $rutfinal;
        break;
      }else{
        return $rutfinal;
        break;
      }
      
    }
  }
  
}

 return "No hay!";

}

private function validaRut($rut){
    if(strpos($rut,"-")==false){
        $RUT[0] = substr($rut, 0, -1);
        $RUT[1] = substr($rut, -1);
    }else{
        $RUT = explode("-", trim($rut));
    }
    $elRut = str_replace(".", "", trim($RUT[0]));
    $factor = 2;
    $suma=0;
    for($i = strlen($elRut)-1; $i >= 0; $i--):
        $factor = $factor > 7 ? 2 : $factor;
        $suma += $elRut{$i}*$factor++;
    endfor;
    $resto = $suma % 11;
    $dv = 11 - $resto;
    if($dv == 11){
        $dv=0;
    }else if($dv == 10){
        $dv="k";
    }else{
        $dv=$dv;
    }
   if($dv == trim(strtolower($RUT[1]))){
       return "true";
   }else{
       return "false";
   }
}
private function encriptaPassword($var) {
    $pass_encriptada1 = md5 ($var); //Encriptacion nivel 1
    $pass_encriptada2 = crc32($pass_encriptada1); //Encriptacion nivel 2
    $pass_encriptada3 = crypt($pass_encriptada2, "xtemp"); //Encriptacion nivel 3
    $pass_encriptada4 = sha1("xtemp".$pass_encriptada3); //Encriptacion nivel 4
    return $pass_encriptada4;
}

function arregla($text){
$text = str_replace("'","`",$text); 
return $text;
}
 
}