<?php 
class conexionUniversal 
{ 
private $_hostMySQL = 'localhost'; 
private $_usuarioMySQL = 'root';
private $_passMySQL = 'Pal0Blanc0';

private $_hostMySQL2 = '10.10.135.60'; 
private $_usuarioMySQL2 = 'root';
private $_passMySQL2 = '163920263a';

private $_hostOracle = '10.10.20.1'; 
private $_baseDatosOracle = 'PROD';
private $_usuarioOracle = 'dm_ventas';
private $_passOracle = 'dimerc'; 

private $_hostPostgres = 'xxx'; 
private $_baseDatosPostgres = 'xxx';
private $_usuarioPostgres = 'xxx';
private $_passPostgres = 'xxx';


private $_coleccion = array();
 

private function _conectar($motor, $db)
{
	switch (strtoupper($motor)) {
		case 'ORACLE':
    		$this->_db = oci_connect($this->_usuarioOracle, $this->_passOracle, $this->_hostOracle."/".$db, 'AL32UTF8');  
			break;
		
		case 'MYSQL':
			$this->_db = mysql_connect($this->_hostMySQL, $this->_usuarioMySQL, $this->_passMySQL);
			if (strpos(strtoupper($_SERVER['REQUEST_URI']),'DESARROLLO') !== false) {
			    //Estoy en Desarrollo
			    $db_selected = mysql_select_db($db."_desa", $this->_db);
		    }else{
			    //Estoy en Productivo
			    $db_selected = mysql_select_db($db, $this->_db);
			}
			break;

		case 'MYSQL_TRISTAN':
			return "LaConsultaNoTieneRegistros";
			$this->_db = mysql_connect($this->_hostMySQL2, $this->_usuarioMySQL2, $this->_passMySQL2);
 			$db_selected = mysql_select_db($db, $this->_db);
			break;

		case 'POSTGRES':
    		$this->_db = pg_connect('host='.$this->_hostPostgres.' dbname='.$db.' user='.$this->_usuarioPostgres.' password='.$this->_passPostgres.'');
			break;
	}
}
 

private function _desconectar($motor, $db)
{
	switch (strtoupper($motor)) {
		case 'ORACLE':
    		oci_close($this->_db);
			break;
		
			case 'MYSQL':
			//case 'MYSQL_TRISTAN':
			    mysql_close($this->_db);
			break;
		case 'POSTGRES':
    		pg_close($this->_db);
			break;
	}
}




 public function selectSimple($query, $motor, $db) 
    { 
 	$this->_conectar($motor, $db);

		switch (strtoupper($motor)) {
			case 'ORACLE':
	    		$result = oci_parse($this->_db, $query); 
			 	oci_execute($result);
			    $fila = oci_fetch_array($result, OCI_ASSOC+OCI_RETURN_NULLS);
			 	   if (empty($fila)) {
					 return "LaConsultaNoTieneRegistros";	
					}
				break;
			
			case 'MYSQL':
				$result = mysql_query($query, $this->_db);
				  	if (mysql_num_rows($result) > 0){
			 	 		$r = mysql_fetch_assoc($result);
				     	$fila = array_map('htmlentities', $r);
			 	 	}else{
				 		return "LaConsultaNoTieneRegistros";	
				 	}
				break;
			case 'MYSQL_TRISTAN':
				 		return "LaConsultaNoTieneRegistros";	
				break;
			case 'POSTGRES':
			 	  $result = pg_query($query);
			 	  if (pg_num_rows($result) > 0){
					 	$fila = pg_fetch_array($result);
				 	}else{
				 		return "LaConsultaNoTieneRegistros";	
				 	}
				break;
		}

 	$this->_desconectar($motor, $db);
	return $fila;
    } 


 public function selectMultiple($query, $motor, $db) 
    { 
 	$this->_conectar($motor, $db);

		switch (strtoupper($motor)) {
			case 'ORACLE':
	    		$result = oci_parse($this->_db, $query); 
 			 	oci_execute($result); 
				  while ($fila = oci_fetch_array($result, OCI_ASSOC+OCI_RETURN_NULLS)) {
		         	 array_push($this->_coleccion, $fila);
				  }
			 	   if (empty($this->_coleccion)) {
				 	return "LaConsultaNoTieneRegistros";	
			 		}
			 	
				break;
			
			case 'MYSQL':
			 	$result = mysql_query($query, $this->_db);
			  	  if (mysql_num_rows($result) > 0){
					 	while($r = mysql_fetch_array($result)) {
						     $fila = array_map('htmlentities', $r);
						      array_push($this->_coleccion, $fila);
							}
				 	}else{
				 	return "LaConsultaNoTieneRegistros";	
				 	}
				break;
			case 'MYSQL_TRISTAN':
				 		return "LaConsultaNoTieneRegistros";	
				break;
			case 'POSTGRES':
			 	  $result = pg_query($query);
			 	  if (pg_num_rows($result) > 0){
					  while($fila = pg_fetch_array($result)) {
					  	array_push($this->_coleccion, $fila);
					  	}
				 	}else{
				 	return "LaConsultaNoTieneRegistros";	
				 	}
				break;
		}
	
 	$this->_desconectar($motor, $db);
	return $this->_coleccion;
    } 



 public function ejecuta($query, $motor, $db) 
    { 
 	$this->_conectar($motor, $db);

		switch (strtoupper($motor)) {
			case 'ORACLE':
	    		$result = oci_parse($this->_db, $query); 
 			 	oci_execute($result); 
				$afectado = oci_num_rows($result);
				break;
			
			case 'MYSQL':
			 	mysql_query($query, $this->_db);
			 	$afectado = mysql_affected_rows();
				break;
			case 'MYSQL_TRISTAN':
				 		return "LaConsultaNoTieneRegistros";	
				break;
			case 'POSTGRES':
			 	  $result = pg_query($query);
			 	  $afectado = pg_affected_rows($result);
				break;
		}
	
 	$this->_desconectar($motor, $db);
 	return $afectado;
     } 

  
 
 public function insertRetornandoId($query, $motor, $db, $campo) 
    { 

 	$this->_conectar($motor, $db);

		switch (strtoupper($motor)) {
			case 'ORACLE':
				$result = oci_parse($this->_db, $query. " returning ".$campo." into :id"); 
				oci_bind_by_name($result,":id",$id,32);
				oci_execute($result); 
				break;
			
			case 'MYSQL':
			 	$result = mysql_query($query, $this->_db);
 	 			$id = mysql_insert_id(); 
				break;
			case 'MYSQL_TRISTAN':
				 		return "LaConsultaNoTieneRegistros";	
				break;
			case 'POSTGRES':
			    return "utiliza el selectSimple y añade returning id a la query";
				break;
		}
	
  
	 	return $id;	
	$this->_desconectar($motor, $db);
    }  

  
}




