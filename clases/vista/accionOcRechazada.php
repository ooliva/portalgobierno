<?php 
$variables = $_GET['var'];

require_once "../modelo/servicio.php";
$serv = new servicio();
$listado = $serv->listadoMotivos();

$dato = explode("*123", $variables);
$idpedido = $dato[0];
$numero = $dato[1];
$ejec = $dato[2]; 
$motivo = $dato[3]; 
$detallemotivo = $dato[4]; 
$cc = $dato[5]; 
?>

 <link rel="stylesheet" href="css/multi-line-button.css" type="text/css">

<style type="text/css">


.titulo{
text-align: center;
}
 

</style>

<a class="popupClose" style="cursor:pointer;">x</a>
	<div >
		<div class="titulo" ><br>Esta cotizacion Nº <?php echo $numero;?> fue rechazada por <?php echo $ejec;?> 

    <br><br> <?php echo "MOTIVO: ".$motivo."<br>COMENTARIO: ".$detallemotivo;?>
		<br><br><br>Como desea proceder?

        <br> 
<table>
<tr>
<td><p onClick='accionRechazo(<?php echo $idpedido;?>, "<?php echo $ejec;?>", 1, "<?php echo $cc;?>")'>
      <a class='multi-line-button' style='width:210px;' >
        <span class='title'>Devolver</span>
        <span class='subtitle'><?php echo "La OC Nº ".$numero." sera devuelto al listado de pendientes del ejecutivo ".$ejec;?></span>
      </a>
    </p></td>
<td><p onClick='accionRechazo(<?php echo $idpedido;?>, "<?php echo $ejec;?>", 2, "<?php echo $cc;?>")'>
      <a class='multi-line-button green' style='width:210px;'>
        <span class='title'>Aceptar</span>
        <span class='subtitle'>Se aprueba y enviara la OC Nº <?php echo $numero;?> a telemarketing, asignandose al ejecutivo <?php echo $ejec;?></span>
      </a>
    </p></td>
<td><p onClick='accionRechazo(<?php echo $idpedido;?>, "<?php echo $ejec;?>", 3, "<?php echo $cc;?>")'>
      <a class='multi-line-button red' style='width:230px;'>
        <span class='title'>Rechazar</span>
        <span class='subtitle'>La OC Nº <?php echo $numero;?> será rechazada definitivamente, desapareciendo de este listado y del ejecutivo.</span>
      </a>
    </p></td>
 
</tr>
</table>
		</div>
	</div>

 