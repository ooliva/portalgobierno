<?php
session_name("robotChilecompra");
@session_start(); 
  if ($_SESSION["autentificado"] == "si"){

 $nombre = $_SESSION['nombre'];
  $finicio = $_SESSION['iniciado'];

if((strtotime(date("Y-n-j H:i:s"))-strtotime($finicio)) >= 600000) {
    
      session_destroy(); 
      echo "Su sesion a expirado<br>";
      echo '<a href="index.php">Volver a acceder</a>';
      exit();
    }

 ?> 
 

 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Robot Chile Compras</title>
 
		<meta name="author" content="Codrops" />
 		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		 <script src="js/jquery.js" type="text/javascript"></script>



<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/demo_page.css" />
<link rel="stylesheet" type="text/css" href="css/demo_table_jui.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.4.custom.css" />


<!-- cargador -->
<script type="text/javascript" src="js/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="js/swfobject.js"></script>

<!-- select -->
 <script src="js/select2.js" type="text/javascript"></script>
    <script src="js/select2.sortable.js" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" href="css/select2.css"/>

<!-- msg -->
 <link rel="stylesheet" type="text/css" href="css/buttons.css"/>
<script type="text/javascript" src="js/jquery.noty.js"></script>
<script type="text/javascript" src="js/layouts/center.js"></script>
<script type="text/javascript" src="js/themes/default.js"></script> 

<!-- imprimir -->
 <script type="text/javascript" src="js/jquery.printElement.min.js"></script>


<style type="text/css">


.backgroundPopup{
  display:none;
  position:fixed;
  _position:absolute; /* hack for internet explorer 6*/
  height:100%;
  width:100%;
  top:0;
  left:0;
  background:#000000;
  border:1px solid #cecece;
  z-index:9998;
}
.popupContent{
  display:none;
  position:fixed;
  _position:absolute; /* hack for internet explorer 6*/
  height:auto;
  width:520px;
  background:#ffffff;
  border:1px solid #000000;
  z-index:9999;
 
  font-size:1.3em;
}
.popupClose{
  font-size:1.4em;
  right:0.2em;
  top:0.1em;
  position:absolute;
  color:#000000;
  font-weight:700;
  display:block;
  margin-top: -8px;
}
.popupClose:hover { cursor: pointer; }

#buscador{
  text-align: center;
}

</style>
<style type="text/css">
    /* demo styles */
    body {font-size: 62.5%; font-family:"Verdana",sans-serif; }
    fieldset { border:0; }
    .selectCenCos,.ui-select-menu { }
    .selectCenCos { width: 200px; }
    .wrap ul.ui-selectmenu-menu-popup li a { font-weight: bold; }
  </style>



<script type="text/javascript">

stringArrayParametros='';
cargarContenido("pendiente_ejecutivo", "pendientes");

function generaNotificacion(type, btn, texto, textobt1, accionbt1, textobt2, accionbt2, funcionbtn1, funcionbtn2, parametros) {
    stringArrayParametros = parametros;
    switch(btn)
    {
    case '0boton':
      var n = noty({
        text: texto,
        type: type,
        dismissQueue: true,
        layout: 'center',
        theme: 'defaultTheme',
        template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation: {
          open: {height: 'toggle'},
          close: {height: 'toggle'},
          easing: 'swing',
          speed: 500 // opening & closing animation speed
        },
        timeout: 1000,
      });
      break;
      case '1boton':
      var n = noty({
        text: texto,
        type: type,
        dismissQueue: true,
        layout: 'center',
        theme: 'defaultTheme',
        template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation: {
          open: {height: 'toggle'},
          close: {height: 'toggle'},
          easing: 'swing',
          speed: 500 // opening & closing animation speed
        },
        timeout: 1000,
   
        buttons: [
          {addClass: 'btn btn-primary', text: textobt1, onClick: function($noty) {
              $noty.close();
              noty({dismissQueue: true, force: true, layout: 'center', theme: 'defaultTheme', text: accionbt1, type: 'success', timeout: 2000});
              llamarFuncion(funcionbtn1);
            }
          }
        ]
      });
      break;
    case '2boton':
      var n = noty({
        text: texto,
        type: type,
        dismissQueue: true,
        layout: 'center',
        theme: 'defaultTheme',
        template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation: {
          open: {height: 'toggle'},
          close: {height: 'toggle'},
          easing: 'swing',
          speed: 500 // opening & closing animation speed
        },
        timeout: 1000,
   
        buttons: [
          {addClass: 'btn btn-primary', text: textobt1, onClick: function($noty) {
              $noty.close();
              noty({dismissQueue: true, force: true, layout: 'center', theme: 'defaultTheme', text: accionbt1, type: 'success', timeout: 2000});
              llamarFuncion(funcionbtn1);
            }
          },
          {addClass: 'btn btn-danger', text: textobt2, onClick: function($noty) {
              $noty.close();
              noty({dismissQueue: true, force: true, layout: 'center', theme: 'defaultTheme', text: accionbt2, type: 'error', timeout: 2000});
              llamarFuncion(funcionbtn2);
            }
          }
        ]
      });
    }
}

  function llamarFuncion(fn) {
 switch(fn)
{
  case 'imprimir':
    variables = stringArrayParametros.split('*123');
    cargarContenido("detalleOCimprmir.php?idpedido="+variables[0]+"&rutejecutivo="+variables[1]+"&desde=imprimir&cencos="+variables[2]+"&cencostext="+variables[3], "imprimir");

setTimeout("imprimir()",1000);
 

    
  break;


  case 'editarOC':
    variables = stringArrayParametros.split('*123');
    cargarContenido("editarRelacionOC.php?idpedido="+variables[0]+"&rutejecutivo="+variables[1]+"&desde=pendientes&cencos="+variables[2]+"&detalle="+variables[3], "pendientes");

    
  break;

 case 'editarOCStock':
    variables = stringArrayParametros.split('*123');
    cargarContenido("editarStockOC.php?idpedido="+variables[0]+"&rutejecutivo="+variables[1]+"&desde=pendientes&cencos="+variables[2]+"&detalle="+variables[3], "pendientes");

  break;




  case 'test':
 
  break;
  }
}   

function imprimir(){

   $('#imprimir').printElement();
}



    $(".selectCenCos").live('change', function() {
      id=this.id;
      partes = id.split("-")

        $("#aceptar-"+partes[1]).css('display', "");
        $("#rechazar-"+partes[1]).css('display', "");
      });

   $("#manual").live('click', function() {
    window.open(
  'http://10.10.127.43/portalGobierno/manual',
  '_blank' // <- This is what makes it open in a new window.
);
       });

   $("#cerrarSesion").live('click', function() {
    window.location.href = "index.html";
      });
//pop up

var popupStatus = 0;
var foco = '';
function loadPopup(){
  //Will only be loaded if the status is 0
  if(popupStatus==0){
    $(".backgroundPopup").css({"opacity": "0.8"});
    $(".backgroundPopup").fadeIn("slow");
    $(".popupContent").fadeIn("slow");
    popupStatus = 1;
    $(".popupContent").html("Cargando... <img src='img/spinner.gif'>");
  }
}

//This will disable the popup when needed
function disablePopup(){
  //Will only disable if status is 1
  if(popupStatus==1){
    $(".backgroundPopup").fadeOut("slow");
    $(".popupContent").fadeOut("slow");
    popupStatus = 0;
    $(".popupContent").html("");
  }
}

//Centers the popup to your window size 
function centerPopup(){
  var windowWidth = document.documentElement.clientWidth;
  var windowHeight = document.documentElement.clientHeight;
  var popupHeight = $(".popupContent").height();
  var popupWidth = $(".popupContent").width();

  $(".popupContent").css({
    "position": "fixed",
    "top": 100,
    "left": myWidth/2-popupWidth/2
  });
  //this is needed for ie6
  $(".backgroundPopup").css({ "height": myHeight });
}
   var myWidth = 0, myHeight = 0;
function alertSize() {

  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }
 
}

function cargaPopUp(tipo, variables){
 alertSize();
 
switch (tipo) {
    case "rechazar":
       url = "clases/vista/formRechazo.php?var="+variables;
       break
 
} 

      ajax = objetoAjax();
 
ajax.open("GET", url, true);  

ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  
 
  qwerty =document.getElementById('popup');
  qwerty.innerHTML = ajax.responseText;  
  switch (tipo) {
     case "rechazar":
 
      break 
     
}
  
  }
 }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
ajax.send(null);
 
 
    centerPopup();
    loadPopup();
 
  //Close popup by clicking the x
  $(".popupClose").live('click', function(){ disablePopup(); });
  //Close popup by clicking outside the box
  $(".backgroundPopup").live('click', function(){ disablePopup(); });
  //Close popup by clicking escape
  $(document).live('keypress', function(e){
    if(e.keyCode==27 && popupStatus==1){
      disablePopup();
    }
  });
}

    $("#listado_procesada").live('click', function() {

      cargarContenido("procesada_ejecutivo", "procesadas");
 
      });


    $("#listado_pendiente").live('click', function() {

      cargarContenido("pendiente_ejecutivo", "pendientes");
 
      });

    $("#listado_rechazada").live('click', function() {

      cargarContenido("rechazada_ejecutivo", "rechazadas");
 
      });

    $("#listado_errores").live('click', function() {

      cargarContenido("error_ejecutivo", "errores");
 
      });

    $("#listado_telemarketing").live('click', function() {

      cargarContenido("telemarketing_ejecutivo", "telemarketing");
 
      });

    $("#cargar_excel").live('click', function() {

      cargarContenido("cargar_excel", foco);
 
      });


    $("#buscador_chc").live('click', function() {

      cargarContenido("buscador_chc", "buscador");
 
      });



function guardarNuevaRelacionOC(idDetalle){


   cod_dimerc = $('#codigoDimerc-'+idDetalle).val();
  unidad = $('#unidad-'+idDetalle).val();

 

  if (cod_dimerc.trim().length==0){
      generaNotificacion('error', '1boton', 'Debe ingresar el codigo Dimerc', 'Entiendo', 'Corriga el error...', 'btn2', 'text2', 'nada', 'nada', 'null');
      return false;
 }

   if (unidad.trim().length==0){
      generaNotificacion('error', '1boton', 'Debe ingresar la unidad de la relacion', 'Entiendo', 'Corriga el error...', 'btn2', 'text2', 'nada', 'nada', 'null');
      return false;
 }

 
 
    var request = $.ajax({
    url: "clases/controlador/valida.php",
    type: "POST",
    data: {funcion: 'guardarNuevaRelacionOC', iddetalle : idDetalle, cod_dimerc : cod_dimerc, unidad : unidad},
    dataType: "html" 
   
  });
        request.done(function(msg) {
              $('#codigoDimerc-'+idDetalle).attr('disabled','disabled');
              $('#unidad-'+idDetalle).attr('disabled','disabled');
              generaNotificacion('success', '1boton', 'Relacion guardada satisfactoriamente', 'Ok', 'Si no faltan mas relaciones puedes aceptar', 'bt2', 'txt2', 'nada', 'nada', null);
             return false;
 
      });
}

 

function aceptarOrden(idPedido, rutEjecutivo, identity){

    cc = $('#cencos-'+identity).val();
    cctext = $('#cencos-'+identity+' option:selected').text();

    var request = $.ajax({
    url: "clases/controlador/valida.php",
    type: "POST",
    data: {funcion: 'aceptarOrden', id : idPedido, rut : rutEjecutivo, cencos : cc},
    dataType: "html" 
   
  });
        request.done(function(msg) {
        
          msgSeparado = msg.split("->");
          switch(msgSeparado[0]) {
    case "bien":
            if (msgSeparado[1]=="Sin_cambios"){
              generaNotificacion('success', '1boton', 'Pedido enviado a telemarketing', 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }else{
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot modifico el descuento por '+msgSeparado[1]+' para que el total de la nota de venta sea igual o menor a la OC original', 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }
            
        break;
    case "bienDuplicado":
            if (msgSeparado[2]=="Sin_cambios"){
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot encontro codigos duplicados, por lo que dividio la OC en '+msgSeparado[1], 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }else{
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot encontro codigos duplicados, por lo que dividio la OC en '+msgSeparado[1]+'. Y ademas el descuento de la OC principal fue modificado a '+msgSeparado[2], 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }
        break;
    case "existe_venta":
              generaNotificacion('error', '1boton', 'Esta Orden ya fue vendida. No puede volver a enviarse', 'Entiendo', 'Vamos a quitarla de tus pendientes...', 'btn2', 'text2', 'nada', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "existe_tlmk":
              generaNotificacion('error', '1boton', 'Esta Orden ya se encuentra en tu bandeja de telemarketing. No puede volver a enviarse', 'Entiendo', 'Vamos a quitarla de tus pendientes...', 'btn2', 'text2', 'nada', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "dolar":
              generaNotificacion('error', '2boton', 'Esta Orden tiene dolares. Por ahora no podemos enviarlo a telemarketing (Estamos trabajando en este error). Por favor haga el ingreso manual, desea imprimir esta OC?', 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Hagalo manual por favor', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "errorDecimal":
                generaNotificacion('error', '2boton', 'El descuento es inferior a 1%. Por ahora no podemos enviarlo a telemarketing (Estamos trabajando en este error). Por favor haga el ingreso manual, desea imprimir esta OC?', 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Hagalo manual por favor', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
                cargarContenido("pendiente_ejecutivo", "pendientes");   
        break;
    case "error_relacion":
                 generaNotificacion('warning', '2boton', 'Esta OC tiene un problema de relacion entre codigo ChC, codigo Dimerc y su unidad. Si lo desea usted puede modificar los codigos y unidades del detalle de esta OC o esperar que el administrador agregue la relacion', 'Si, quiero modificar ahora!', 'Vamos a editar...', 'No, Esperare', 'Intentelo mas tarde', 'editarOC', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc+'*123'+msgSeparado[1]);
         break;
    case "error_stock":
                 generaNotificacion('warning', '2boton', 'Esta OC tiene un problema de stock. Si lo desea usted puede reemplazar los items con quiebre o esperar a que el stock sea suiciente', 'Si, quiero modificar ahora!', 'Vamos a editar...', 'No, Esperare', 'Intentelo mas tarde', 'editarOCStock', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc+'*123'+msgSeparado[1]);
         break;
    case "error_deshabilitado":
                generaNotificacion('warning', '1boton', 'En estos momentos estamos cargando la nueva paleta de productos, intentalo de nuevo en 5 minutos', 'Entiendo', 'intentalo mas tarde...', 'btn2', 'text2', 'nada', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
         break;   
    default:
                generaNotificacion('error', '2boton', msg, 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Enviando a pestaña error', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
                cargarContenido("pendiente_ejecutivo", "pendientes");  
}
 

      });
}

function aceptarOrdenDesdeDetalle(idPedido, rutEjecutivo, cen){

    if (cen!="vacio"){
          cc = cen;
    }else{
          cc = $('#cencos-0').val();
          cctext = $('#cencos-0 option:selected').text();

    }


    var request = $.ajax({
    url: "clases/controlador/valida.php",
    type: "POST",
    data: {funcion: 'aceptarOrden', id : idPedido, rut : rutEjecutivo, cencos : cc},
    dataType: "html" 
   
  });
        request.done(function(msg) {
 
 
          msgSeparado = msg.split("->");
          switch(msgSeparado[0]) {
    case "bien":
            if (msgSeparado[1]=="Sin_cambios"){
              generaNotificacion('success', '1boton', 'Pedido enviado a telemarketing', 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }else{
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot modifico el descuento por '+msgSeparado[1]+' para que el total de la nota de venta sea igual o menor a la OC original', 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }
            
        break;
    case "bienDuplicado":
            if (msgSeparado[2]=="Sin_cambios"){
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot encontro codigos duplicados, por lo que dividio la OC en '+msgSeparado[1], 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }else{
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot encontro codigos duplicados, por lo que dividio la OC en '+msgSeparado[1]+'. Y ademas el descuento de la OC principal fue modificado a '+msgSeparado[2], 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }
        break;
    case "existe_venta":
              generaNotificacion('error', '1boton', 'Esta Orden ya fue vendida. No puede volver a enviarse', 'Entiendo', 'Vamos a quitarla de tus pendientes...', 'btn2', 'text2', 'nada', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "existe_tlmk":
              generaNotificacion('error', '1boton', 'Esta Orden ya se encuentra en tu bandeja de telemarketing. No puede volver a enviarse', 'Entiendo', 'Vamos a quitarla de tus pendientes...', 'btn2', 'text2', 'nada', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "dolar":
              generaNotificacion('error', '2boton', 'Esta Orden tiene dolares. Por ahora no podemos enviarlo a telemarketing (Estamos trabajando en este error). Por favor haga el ingreso manual, desea imprimir esta OC?', 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Hagalo manual por favor', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "errorDecimal":
                generaNotificacion('error', '2boton', 'El descuento es inferior a 1%. Por ahora no podemos enviarlo a telemarketing (Estamos trabajando en este error). Por favor haga el ingreso manual, desea imprimir esta OC?', 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Hagalo manual por favor', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
                cargarContenido("pendiente_ejecutivo", "pendientes");   
        break;
    case "error_relacion":
                 generaNotificacion('warning', '2boton', 'Esta OC tiene un problema de relacion entre codigo ChC, codigo Dimerc y su unidad. Si lo desea usted puede modificar los codigos y unidades del detalle de esta OC o esperar que el administrador agregue la relacion', 'Si, quiero modificar ahora!', 'Vamos a editar...', 'No, Esperare', 'Intentelo mas tarde', 'editarOC', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc+'*123'+msgSeparado[1]);
         break;
    case "error_stock":
                 generaNotificacion('warning', '2boton', 'Esta OC tiene un problema de stock. Si lo desea usted puede reemplazar los items con quiebre o esperar a que el stock sea suiciente', 'Si, quiero modificar ahora!', 'Vamos a editar...', 'No, Esperare', 'Intentelo mas tarde', 'editarOCStock', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc+'*123'+msgSeparado[1]);
         break;
    default:
                generaNotificacion('error', '2boton', msg, 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Enviando a pestaña error', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
                cargarContenido("pendiente_ejecutivo", "pendientes");  
}
 

      });
}

function aceptarOrdenDesdeRechazada(idPedido, rutEjecutivo, cen){

 
    var request = $.ajax({
    url: "clases/controlador/valida.php",
    type: "POST",
    data: {funcion: 'aceptarOrden', id : idPedido, rut : rutEjecutivo, cencos : cen},
    dataType: "html" 
   
  });
        request.done(function(msg) {
 
 
          msgSeparado = msg.split("->");
          switch(msgSeparado[0]) {
    case "bien":
            if (msgSeparado[1]=="Sin_cambios"){
              generaNotificacion('success', '1boton', 'Pedido enviado a telemarketing', 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }else{
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot modifico el descuento por '+msgSeparado[1]+' para que el total de la nota de venta sea igual o menor a la OC original', 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }
            
        break;
    case "bienDuplicado":
            if (msgSeparado[2]=="Sin_cambios"){
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot encontro codigos duplicados, por lo que dividio la OC en '+msgSeparado[1], 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }else{
              generaNotificacion('warning', '1boton', 'Pedido enviado a telemarketing, pero el robot encontro codigos duplicados, por lo que dividio la OC en '+msgSeparado[1]+'. Y ademas el descuento de la OC principal fue modificado a '+msgSeparado[2], 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'nada', null);
              cargarContenido("pendiente_ejecutivo", "pendientes");
            }
        break;
    case "existe_venta":
              generaNotificacion('error', '1boton', 'Esta Orden ya fue vendida. No puede volver a enviarse', 'Entiendo', 'Vamos a quitarla de tus pendientes...', 'btn2', 'text2', 'nada', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "existe_tlmk":
              generaNotificacion('error', '1boton', 'Esta Orden ya se encuentra en tu bandeja de telemarketing. No puede volver a enviarse', 'Entiendo', 'Vamos a quitarla de tus pendientes...', 'btn2', 'text2', 'nada', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "dolar":
              generaNotificacion('error', '2boton', 'Esta Orden tiene dolares. Por ahora no podemos enviarlo a telemarketing (Estamos trabajando en este error). Por favor haga el ingreso manual, desea imprimir esta OC?', 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Hagalo manual por favor', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "errorDecimal":
                generaNotificacion('error', '2boton', 'El descuento es inferior a 1%. Por ahora no podemos enviarlo a telemarketing (Estamos trabajando en este error). Por favor haga el ingreso manual, desea imprimir esta OC?', 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Hagalo manual por favor', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
                cargarContenido("pendiente_ejecutivo", "pendientes");   
        break;
    case "error_relacion":
                 generaNotificacion('warning', '2boton', 'Esta OC tiene un problema de relacion entre codigo ChC, codigo Dimerc y su unidad. Si lo desea usted puede modificar los codigos y unidades del detalle de esta OC o esperar que el administrador agregue la relacion', 'Si, quiero modificar ahora!', 'Vamos a editar...', 'No, Esperare', 'Intentelo mas tarde', 'editarOC', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc+'*123'+msgSeparado[1]);
         break;
    case "error_stock":
                 generaNotificacion('warning', '2boton', 'Esta OC tiene un problema de stock. Si lo desea usted puede reemplazar los items con quiebre o esperar a que el stock sea suiciente', 'Si, quiero modificar ahora!', 'Vamos a editar...', 'No, Esperare', 'Intentelo mas tarde', 'editarOCStock', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc+'*123'+msgSeparado[1]);
         break;
    default:
                generaNotificacion('error', '2boton', msg, 'Si, Imprimir Ahora', 'Imprimiendo...', 'No, Imprimir Despues', 'Enviando a pestaña error', 'imprimir', 'nada', idPedido+'*123'+rutEjecutivo+'*123'+cc);
                cargarContenido("pendiente_ejecutivo", "pendientes");  
}
 

      });
}


function rechazarOrden(idPedido, rutEjecutivo, cc, cctext, identity){

sel = $('#selectMotivo').val();

if (identity=="vacio"){
    cc = cc;
  cctext2 = cctext;

}else{
  cc = $('#cencos-'+identity).val();
  cctext2 = $('#cencos-'+identity+' option:selected').text();
}
 
 
 if (sel>1){
    com = $('#comentario').val();
    if (com.trim().length==0){
      alert("Debe escribir un comentario!");

    }else{
          var request = $.ajax({
        url: "clases/controlador/valida.php",
        type: "POST",
        data: {funcion: 'rechazar', id : idPedido, rut : rutEjecutivo, idmotivo : sel, comentario : com, cencos : cc, cctext : cctext2},
        dataType: "html" 
       
      });
       request.done(function(msg) {
        if (msg=="go") {
           generaNotificacion('success', '1boton', 'Cotizacion rechazada', 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'func2', null);
           cargarContenido("pendiente_ejecutivo", "pendientes");
          disablePopup();
         }else{
           generaNotificacion('error', '1boton', msg, 'Ok', 'Actualizando listado...', 'bt2', 'txt2', 'nada', 'func2', null);
         }
         });
    }
 
}else{

  alert("Debe elegir un motivo!");
}

}

function verOC(idPedido, rutEjecutivo, donde, contador){
  if((donde=="pendientes") || (donde=="rechazadas")){
      cc = $("#cencos-"+contador).val();
      cctext = $('#cencos-'+contador+' option:selected').text();
  }else{
    cc="vacio";
    cctext="vacio";
  }
      
    cargarContenido("detalleOC.php?idpedido="+idPedido+"&rutejecutivo="+rutEjecutivo+"&desde="+donde+"&cencos="+cc+"&cencostext="+cctext, donde);
  
}

function objetoAjax(){
    var xmlhttp=false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
       xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
      }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}


function cargarContenido(tipo, lugar) {



$("#"+lugar).html("<div style='text-align:center;'><img src='img/espere2.gif'></div>");

foco = lugar;
      ajax = objetoAjax();
 
switch (tipo) {
    case "pendiente_ejecutivo":
       variable = "clases/vista/oc_pendiente_ejecutivo.php";
       break
    case "rechazada_ejecutivo":
       variable = "clases/vista/oc_rechazada_ejecutivo.php";
       break
    case "procesada_ejecutivo":
       variable = "clases/vista/oc_procesada_ejecutivo.php";
       break
    case "error_ejecutivo":
       variable = "clases/vista/oc_error_ejecutivo.php";
       break
    case "cargar_excel":
       variable = "clases/vista/cargar_excel.php";
       break
    case "telemarketing_ejecutivo":
       variable = "clases/vista/telemarketing_ejecutivo.php";
       break
    case "buscador_chc":
       variable = "clases/vista/frame_buscador_chc.php";
       break

    default:
       variable = "clases/vista/"+tipo;
       break    


} 
 
ajax.open("GET",variable, true);  
 
ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  
 
  qwerty =document.getElementById(lugar);
  qwerty.innerHTML = ajax.responseText;  
 switch (tipo) {
     case "pendiente_ejecutivo":
 			listenersPendiente();
        break 
     case "rechazada_ejecutivo":
      listenersRechazada();
        break  
     case "error_ejecutivo":
      listenersError();
        break  
     case "procesada_ejecutivo":
      listenersProcesada();
        break 
     case "cargar_excel":
      listenersCargar();
        break
     case "telemarketing_ejecutivo":
      listenersTelemarketing();
        break  
    default:
     listenersDetalle();
      listenerseditarOCStock();

       break
 
} 
 
  }
 }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
ajax.send(null);
 
}

var prevNowPlaying = null;

function listenersDetalle(){

 $(document).ready(function() {
    $("#cencos-0").live('change', function() {

        $("#aceptar").css('display', "");
        $("#rechazar").css('display', "");
 
      });
     if(prevNowPlaying) {
        clearInterval(prevNowPlaying);
    }
    prevNowPlaying  = setInterval(function () {
    $(".recordatorio").css("color", function () {
        this.switch = !this.switch
        return this.switch ? "red" : ""
    });
}, 100)
 
   });
 console.log("aaa");
}

function listenersCargar(){
 $(document).ready(function() {
  $('#fileInputBalance').uploadify({
    'uploader'  : 'img/uploadify.swf',
    'script'    : 'clases/controlador/cargadorExcel.php',
    'cancelImg' : 'img/subiendo.gif',
    'auto'      : true,
    'folder'    : 'clases/uploads',
    'scriptData' : {'texto': $("#mitexto").val()},
    'onSelect' : function(file) {
            $('#respuestaBalance').html("");
        },
    'onComplete': function(event, queueID, fileObj, response, data) {
        $('#respuestaBalance').html(response);
    }
  });
 
});
}

function listenersPendiente(){

    // oTable = $('#tablaPendiente').dataTable({
    //   "aaSorting": [[ 1, "asc" ]],
    //     "bJQueryUI": true,
    //     "sPaginationType": "full_numbers",
    //     "oLanguage": {
    //         "sProcessing": "Espere Un Momento...",
    //         "sLengthMenu": "Mostrar _MENU_ OC Pendientes",
    //         "sZeroRecords": "No Hay Coincidencias en la Busqueda",
    //         "sInfo": "Mostrando del _START_ hasta _END_ de un total de _TOTAL_ OC",
    //         "sInfoEmpty": "Mostrando 0 de 0 de un total de 0 OC",
    //         "sInfoFiltered": "(Coincidencias en busqueda)",
    //         "sInfoPostFix": "",
    //         "sSearch": "Buscar",
    //         "sUrl": "",
    //         "oPaginate": {
    //           "sFirst":    "Primera",
    //           "sPrevious": "Anterior",
    //           "sNext":     "Siguente",
    //           "sLast":     "Ultima"

    //             }
    //       }
    // });

    $('.bloqueado').css({ opacity: 0.3 });
    $(function(){
 
 
      $('.selectCenCos').live('change', function() {
        id = this.id;
        id = id.split("-");
        cc = $(this).val();
        rut = $(this).attr('name');
        
           var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'dameEstado', rut : rut, cencos : cc},
            dataType: "html" 
           
          });
           request.done(function(msg) {
                $('#estado-'+id[1]).html(msg);
 
             });
      });    

          $(".selectCenCos").select2({
      width: '210px'
    });


      // $('.selectCenCos').selectmenu({
      //   style:'popup',
      //   width: 300,
      //   format: addressFormatting,
      //   theme:'Dot Luv'
      // });

 
    });

    //a custom format option callback
    var addressFormatting = function(text, opt){
      var newText = text;
      //array of find replaces
      var findreps = [
        {find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
        {find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
        {find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
        {find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
        {find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
      ];

      for(var i in findreps){
        newText = newText.replace(findreps[i].find, findreps[i].rep);
      }
      return newText;
    }
}

    

function listenersError(){

    oTable = $('#tablaError').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sProcessing": "Espere Un Momento...",
            "sLengthMenu": "Mostrar _MENU_ OC Rechazadas",
            "sZeroRecords": "No Hay Coincidencias en la Busqueda",
            "sInfo": "Mostrando del _START_ hasta _END_ de un total de _TOTAL_ OC",
            "sInfoEmpty": "Mostrando 0 de 0 de un total de 0 OC",
            "sInfoFiltered": "(Coincidencias en busqueda)",
            "sInfoPostFix": "",
            "sSearch": "Buscar",
            "sUrl": "",
            "oPaginate": {
              "sFirst":    "Primera",
              "sPrevious": "Anterior",
              "sNext":     "Siguente",
              "sLast":     "Ultima"

                }
          }
    });

 
 } 

function listenersTelemarketing(){


    oTable = $('#tablaTelemarketing').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sProcessing": "Espere Un Momento...",
            "sLengthMenu": "Mostrar _MENU_ OC Rechazadas",
            "sZeroRecords": "No Hay Coincidencias en la Busqueda",
            "sInfo": "Mostrando del _START_ hasta _END_ de un total de _TOTAL_ OC",
            "sInfoEmpty": "Mostrando 0 de 0 de un total de 0 OC",
            "sInfoFiltered": "(Coincidencias en busqueda)",
            "sInfoPostFix": "",
            "sSearch": "Buscar",
            "sUrl": "",
            "oPaginate": {
              "sFirst":    "Primera",
              "sPrevious": "Anterior",
              "sNext":     "Siguente",
              "sLast":     "Ultima"

                }
          }
    });

 
 } 

function listenersProcesada(){

    oTable = $('#tablaProcesada').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sProcessing": "Espere Un Momento...",
            "sLengthMenu": "Mostrar _MENU_ OC Rechazadas",
            "sZeroRecords": "No Hay Coincidencias en la Busqueda",
            "sInfo": "Mostrando del _START_ hasta _END_ de un total de _TOTAL_ OC",
            "sInfoEmpty": "Mostrando 0 de 0 de un total de 0 OC",
            "sInfoFiltered": "(Coincidencias en busqueda)",
            "sInfoPostFix": "",
            "sSearch": "Buscar",
            "sUrl": "",
            "oPaginate": {
              "sFirst":    "Primera",
              "sPrevious": "Anterior",
              "sNext":     "Siguente",
              "sLast":     "Ultima"

                }
          }
    });

 
 } 


function listenersRechazada(){

    oTable = $('#tablaRechazada').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sProcessing": "Espere Un Momento...",
            "sLengthMenu": "Mostrar _MENU_ OC Procesadas",
            "sZeroRecords": "No Hay Coincidencias en la Busqueda",
            "sInfo": "Mostrando del _START_ hasta _END_ de un total de _TOTAL_ OC",
            "sInfoEmpty": "Mostrando 0 de 0 de un total de 0 OC",
            "sInfoFiltered": "(Coincidencias en busqueda)",
            "sInfoPostFix": "",
            "sSearch": "Buscar",
            "sUrl": "",
            "oPaginate": {
              "sFirst":    "Primera",
              "sPrevious": "Anterior",
              "sNext":     "Siguente",
              "sLast":     "Ultima"

                }
          }
    });

    $(function(){
 

    //  $(".selectCenCos").select2({
    //   width: '210px'
    // });

 
    });

    //a custom format option callback
    var addressFormatting = function(text, opt){
      var newText = text;
      //array of find replaces
      var findreps = [
        {find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
        {find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
        {find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
        {find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
        {find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
      ];

      for(var i in findreps){
        newText = newText.replace(findreps[i].find, findreps[i].rep);
      }
      return newText;
    }
} 

function listenerseditarOCStock(){

    $(".selectSug").select2({
      width: '920px'
    });
  
}

function limpiarPendientes(){
  $("#pendientes").html("<div style='text-align:center;'><img src='img/cleanup.gif'><br>Estamos limpiando todas aquellas cotizaciones que ya se vendieron o estan procesadas...</div>");

           var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'limpiarPendientes'},
            dataType: "html" 
           
          });
           request.done(function(msg) {
                cargarContenido("pendiente_ejecutivo", "pendientes");
 
             });
}

function buscarOCtelemarketing(){
 numero = $("#oc_tlmk").val();
            var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'buscarOCtelemarketing', numero : numero},
            dataType: "json" 
           
          });
           request.done(function(msg) {

            switch (msg["resultado"]) {
                case "existe":
                        if (msg["estado"] == "borrable"){
                           $("#respuesta_telemarketing").html("<table style='border: 1px solid black; width:600px; margin-left:300px;'><tr><td>OC</td><td>Rut</td><td>CenCos</td><td>Descuento</td><td>Fecha</td></tr><tr><td>"+msg["facnom"]+"</td><td>"+msg["rutcli"]+"</td><td>"+msg["cencos"]+"</td><td>"+msg["descli"]+"</td><td>"+msg["fecord"]+"</td></tr></table><br><img style='margin-left:230px;' onClick='borrarOCtelemarketing("+numero+");' src='img/trash_64x64.png'>");
                        }else{
                          $("#respuesta_telemarketing").html("<table style='border: 1px solid black; width:600px; margin-left:300px;'><tr><td>OC</td><td>Rut</td><td>CenCos</td><td>Descuento</td><td>Fecha</td></tr><tr><td>"+msg["facnom"]+"</td><td>"+msg["rutcli"]+"</td><td>"+msg["cencos"]+"</td><td>"+msg["descli"]+"</td><td>"+msg["fecord"]+"</td></tr></table><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Esta OC tiene asociada una nota de venta. No puedes borrarla...");
                        }
                   break
                case "error":
                    $("#respuesta_telemarketing").html("<div style='margin-left:200px;'>Solo puedes ingresar numeros enteros...<br>Intentelo nuevamente</div>");
                   break
                default:
                   $("#respuesta_telemarketing").html("<div style='margin-left:200px;'>No fue posible encontrar la OC solicitada...<br>Intentelo nuevamente</div>");
                   break    
            } 
 
          });
}

function borrarOCtelemarketing(num){
             var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'borrarOCtelemarketing', numero : num},
            dataType: "html" 
           
          });
           request.done(function(msg) {
               $("#respuesta_telemarketing").html("<div style='margin-left:200px;'><img src='img/Check.png'><br>Cotizacion quitada satisfactoriamente de su bandeja de telemarketing</div>");
          
 
             });
}

function apurar(){
  $("#divBtn3").toggle("slow"); 
  $("#divBtn4").toggle("slow");
}

function traerOCahora(){
 oc = $("#oc_ahora").val(); 
 if (oc.trim().length==0){
      generaNotificacion('error', '1boton', 'Debe ingresar el numero de la orden de compra', 'Entiendo', 'No actualizaremos...', 'btn2', 'text2', 'nada', 'nada', 'null');
      return false;
 }

  $("#botonAhora").hide(); 
 $("#botonTiempo").show(); 

              var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'traerOCahora', oc : oc},
            dataType: "html" 
           
          });
        request.done(function(msg) {
          msgSeparado = msg.split("-");
          switch(msgSeparado[0]) {
    case "0":
              generaNotificacion('warning', '1boton', 'Esta orden no es convenio marco', 'Entiendo', 'No actualizaremos...', 'btn2', 'text2', 'nada', 'nada', 'null');            
        break;
    case "1":
              generaNotificacion('warning', '1boton', 'Esta Orden ya fue leida por el robot y asignada a '+msgSeparado[1], 'Entiendo', 'No actualizaremos...', 'btn2', 'text2', 'nada', 'nada', 'null');
        break;
    case "2":
              generaNotificacion('warning', '1boton', 'Esta Orden ya se encuentra en la bandeja de telemarketing', 'Entiendo', 'No actualizaremos...', 'btn2', 'text2', 'nada', 'nada', 'null');
        break;
    case "3":
              generaNotificacion('success', '1boton', 'El robot a encontrado la OC y la ha asignado a '+msgSeparado[1], 'Que bien', 'Actualizando listado...', 'btn2', 'text2', 'nada', 'nada', 'null');
              cargarContenido("pendiente_ejecutivo", "pendientes");
        break;
    case "4":
                generaNotificacion('error', '1boton', 'El robot encontro la OC, pero no pudo determinar al ejecutivo. Se le envio al Administrador de este sistema. Contactelo si considera que es de usted.', 'Entiendo', 'No actualizaremos...', 'btn2', 'text2', 'nada', 'nada', 'null');
        break;
    case "no_existe":
                generaNotificacion('error', '1boton', 'El robot no pudo encontrar esta OC, revise el numero ingresado o notifique a Eduardo', 'Entiendo', 'No actualizaremos...', 'btn2', 'text2', 'nada', 'nada', 'null');
        break;
    default:
                generaNotificacion('error', '1boton', 'Un error ha ocurrido, notifique a Eduardo', 'Entiendo', 'No actualizaremos...', 'btn2', 'text2', 'nada', 'nada', 'null');
}
   $("#botonAhora").show(); 
 $("#botonTiempo").hide(); 

      });

}

function guardarReemplazo(id, nuevoCodigo, anteriorCodigo, iterador, tipo, cantidad, stock2){

if($("#sug"+name).iterador == 0) {
  generaNotificacion('warning', '1boton', 'Debe elegir una familia y despues un item para reemplazar a '+anteriorCodigo, 'Ok', 'Elija un producto...', 'btn2', 'text2', 'nada', 'nada', 'null');
   return false;
}

 if (tipo!="sugerencia"){
  nuevoCodigo = $('#sug-'+iterador).val();
  nuevoTexto = $('#sug-'+iterador+" option:selected").text();
  unidad = $('#unidadList-'+iterador).val();
   if (nuevoCodigo=="vacio"){
  generaNotificacion('warning', '1boton', 'Debe seleccionar un producto del listado para reemplazar a '+anteriorCodigo, 'Ok', 'Elija un producto...', 'btn2', 'text2', 'nada', 'nada', 'null');
   return false;
   }else{
      stock = $('#sug-'+iterador+' option:selected').attr("name");
      if (cantidad>stock){
         if (confirm('El producto seleccionado tambien tiene stock insuficiente. Solo tiene '+stock+' items. Usarlo de todas formas?')) {
                   
                } else {
                return false;
                }
       }
   }
 }else{
        stock = stock2;
      if (cantidad>stock){
         if (confirm('El producto sugerido tambien tiene stock insuficiente. Solo tiene '+stock+' items. Usarlo de todas formas?')) {
                   
                } else {
                return false;
                }
       }
  unidad = $('#unidadSug-'+iterador).val();  
 }
 
 if (unidad.trim().length==0){
      generaNotificacion('error', '1boton', 'Debe ingresar la unidad de venta', 'Entiendo', 'Ingrese la unidad', 'btn2', 'text2', 'nada', 'nada', 'null');
      return false;
 }

          
              var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'reemplazarProducto', iddetalle : id, cod_dimerc : nuevoCodigo, cod_dimerc_old : anteriorCodigo, tipo : tipo, unidad : unidad, nuevoTexto : nuevoTexto},
            dataType: "html" 
           
          });
        request.done(function(msg) {
               
           $('#filaSug-'+iterador).hide(500);
           $('#filaList-'+iterador).hide(500);
           $('#cantTlmk-'+iterador).hide(500);
           $('#stock-'+iterador).hide(500);
           $('#un-'+iterador).hide(500);
           $('#msg-'+iterador).hide(500);
           $('#filaOriginal-'+iterador).css("color", "#47a3da");
           $('#foto-'+iterador).attr("src", "img/ok.png");
           $('#descripcion-'+iterador).html(nuevoTexto);
           $('#codigo-'+iterador).html(nuevoCodigo);
           $('#codigoChc-'+id).html("reemplazado");
      });

}


function traerListadoFamilia(iterador, precio, rut, cencos){

 $('#tdsug-'+iterador).html("<div style='text-align:center;'><img style='width:90px;' src='img/cargando.gif'></div>");
          
        var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'traerListadoFamilia'},
            dataType: "json" 
           
          });
        request.done(function(msg) {

          str = "<select class='selectFamilia' id='fam-"+iterador+"'><option disabled selected value='vacio'>Aqui esta el listado completo de las familias...</option>";
           for (var i =0; i < msg.length; i++) {
               fila = msg[i];
                 str += "<option value='"+fila['FAMILIA']+"'>"+fila['FAMILIA']+"</option>"; 
             };
            str +="</select>"; 

            str += '<img src="img/este.png" style="width:35px; margin-left:20px; cursor:pointer;" title="Traer todos los productos de esta familia" onClick="traerTodoDeEstaFamilia('+iterador+', '+precio+', '+rut+', '+cencos+');">';
            str += '<img src="img/error2.png" style="width:70px; margin-left:10px; cursor:pointer;" title="Traer todos los productos" onClick="traerListadoFamilia('+iterador+', '+precio+', '+rut+', '+cencos+');">';
           $('#tdsug-'+iterador).html(str);

                $('#fam-'+iterador).select2({
                  width: '600px'
                });
      });

}

function traerTodoDeEstaFamilia(iterador, precio, rut, cencos){

  familia = $('#fam-'+iterador).val();

 $('#tdsug-'+iterador).html("<div style='text-align:center;'><img style='width:90px;' src='img/cargando.gif'></div>");
          
        var request = $.ajax({
            url: "clases/controlador/valida.php",
            type: "POST",
            data: {funcion: 'traerTodoDeEstaFamilia', precio : precio, familia : familia, rut : rut, cencos : cencos},
            dataType: "json" 
           
          });
        request.done(function(msg) {

      

          str = "<select class='selectSug' id='sug-"+iterador+"'><option disabled selected value='vacio'>Aqui esta el listado de la familia "+familia+"...</option>";
           for (var i =0; i < msg.length; i++) {
               fila = msg[i];
                 str += "<option name='"+fila['STOCK']+"' value='"+fila['CODPRO']+"'>"+fila['CODPRO']+"=> "+fila['DESPRO']+" (PRECIO: $"+fila['PRECIO']+")</option>"; 
             };
            str +="</select>"; 

            str += '<img src="img/error2.png" style="width:70px; margin-left:10px; cursor:pointer;" title="Traer todos los productos" onClick="traerListadoFamilia('+iterador+', '+precio+');">';
           $('#tdsug-'+iterador).html(str);

                $('#sug-'+iterador).select2({
                  width: '900px'
                });
      });

}

</script>
 

	</head>
	<body>
		<div class="container">
			<header class="clearfix">
				<span>Robot Chile Compras</span>
				<h1>Bienvenido: <?php echo $nombre; ?></h1>
				<nav>
					<a href="#" class="bp-icon bp-icon-prev" data-info="Configurar"><span>Configurar</span></a>
					<a href="#" class="bp-icon bp-icon-next" data-info="Manual de Usuario" id="manual"><span>Manual de Usuario</span></a>
					<a href="#" class="bp-icon bp-icon-archive" id="cerrarSesion" data-info="Cerrar sesion"><span>Cerrar sesion</span></a>
				</nav>
			</header>	
			<div id="tabs" class="tabs">
				<nav>
					<ul>
						<li><a href="#pendientes" id="listado_pendiente" class="icon-shop"><span>OC pendientes</span></a></li>
						<li><a href="#rechazadas" id="listado_rechazada" class="icon-cup"><span>OC rechazadas</span></a></li>
            <li><a href="#errores" id="listado_errores" class="icon-error"><span>OC errores</span></a></li>
						<li><a href="#procesadas" id="listado_procesada" class="icon-truck"><span>OC procesadas</span></a></li>
            <li><a href="#buscador" id="buscador_chc" class="icon-lab"><span>Buscador ChC</span></a></li>
<!--             <li><a href="#telemarketing" id="listado_telemarketing" class="icon-lab"><span>Telemarketing</span></a></li>
 -->					</ul>
				</nav>
				<div class="content">
					<section id="pendientes">
 
					</section>
					<section id="rechazadas">
 
					</section>
          <section id="errores">
 
          </section>
					<section id="procesadas">
						 
					</section>
          <section id="buscador">
 
          </section>
					<section id="telemarketing">
 
					</section>
 
				</div><!-- /content -->
			</div><!-- /tabs -->
			 
		</div>
		<script src="js/cbpFWTabs.js"></script>
		<script>
			new CBPFWTabs( document.getElementById( 'tabs' ) );
		</script>


    <div class="popupContent" id="popup">

                        </div>
 
                         <div class="backgroundPopup"></div>


<style type="text/css">
   #imprimir{display:none;}
 
    @media print {
     #noprint{display: none;}
     #print_this {
       display: block;
     }
    }
  </style>  

      <div id="imprimir">

                        </div>
                       
	</body>
</html>
<?php
 }else{
header ("Location: index.html");
}   

 ?>