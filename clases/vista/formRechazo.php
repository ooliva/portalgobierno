<?php 
$variables = $_GET['var'];

require_once "../modelo/servicio.php";
$serv = new servicio();
$listado = $serv->listadoMotivos();

$dato = explode("*123", $variables);
$idpedido = $dato[0];
$rutejecutivo = $dato[1]; 
$cc = $dato[2];
$cctext = $dato[3]; 
$identity = $dato[4]; 
?>
<style type="text/css">


.titulo{
text-align: center;
}
 
.area{
	margin-top: 20px;
width:400px;
height:80px;
resize: none;
}
#selectMotivo {
	width:400px;
    padding:3px;
    margin: 0;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    background: #f8f8f8;
    color:#888;
    border:none;
    outline:none;
    display: inline-block;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
    background-image: url('img/bkg-dropdown-arrow.png');
    background-repeat: no-repeat;
    background-position: 370px 3px; 
    margin-top: 10px;
}

.btn{
margin-top: 10px;
width: 50px;
cursor: pointer;
} 

</style>

<a class="popupClose" style="cursor:pointer;">x</a>
	<div >
		<div class="titulo" >Por qué va a rechazar esta cotización? 

		<select id="selectMotivo">
			<option value="" disabled selected>Elija un motivo...</option>
			<?php
			for ($i=0; $i < count($listado); $i++) { 
				$fila = $listado[$i];
				echo "<option value='".$fila[0]."'>".$fila[1]."</option>";
			}
			?> 
		</select>
		<textarea class="area" id="comentario"></textarea><br>
		<img src="img/rejection_ideogram2.png" class="btn" id="rechazarCotizacion" onClick='rechazarOrden(<?php echo $idpedido;?>, <?php echo $rutejecutivo;?>, "<?php echo $cc;?>", "<?php echo $cctext;?>", "<?php echo $identity;?>");'>
		</div>
	</div>

 